#pragma once

#ifndef RECLINER_ASYNC_CONTROLLER_HPP_
#define RECLINER_ASYNC_CONTROLLER_HPP_

#include "purpl/async_wrapper.hpp"
#include "purpl/math.hpp"
#include "subsystems/recliner.hpp"

using namespace purpl::literals;

template <class M>
class ReclinerAsyncController :
    public purpl::AsyncWrapper<ReclinerAsyncController<M>>,
    public Recliner<M> {
public:
    ReclinerAsyncController(Recliner<M>&& recliner) :
        purpl::AsyncWrapper<ReclinerAsyncController<M>>(
                this, "ReclinerAsyncController"),
        Recliner<M>{std::move(recliner)} {}

    void loop() {
        while (true) {
            Recliner<M>::step();
            pros::delay(20);
        }
    }

    void wait_settled(
            purpl::QAngle tol = DEFAULT_TILT_TOLERANCE,
            purpl::QTime duration = DEFAULT_SETTLE_DURATION) {
        const std::uint32_t dur_millis = duration.convert(1_ms);
        purpl::make_wait_settled_func(LOOP_DELAY, dur_millis, tol, [&]() {
            switch (state) {
            case ReclinerState::forward:
                return forward_max - get_potentiometer_angle();
            case ReclinerState::backward:
                return backward_max - get_potentiometer_angle();
            default:
                return 0 * purpl::degree;
            }
        });
    }

    using Recliner<M>::get_potentiometer_angle;

private:
    static constexpr std::uint32_t LOOP_DELAY = 20;
    static constexpr purpl::QAngle DEFAULT_TILT_TOLERANCE = 1_deg;
    static constexpr purpl::QTime DEFAULT_SETTLE_DURATION = 0.2_s;

    using Recliner<M>::forward_max;
    using Recliner<M>::backward_max;
    using Recliner<M>::state;
};

#endif
