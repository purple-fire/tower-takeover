/**
 * @file
 * Potentiometer class which scales readings into QAngle values.
 */

#pragma once

#ifndef PURPL_POTENTIOMETER_HPP_
#define PURPL_POTENTIOMETER_HPP_

#include "pros/adi.hpp"
#include "units.hpp"

namespace purpl {

/**
 * Uncalibrated potentiometer.
 *
 * This returns absolute readings from the device's zero position, regardless of
 * its position at startup.
 */
class Pot : protected pros::ADIPotentiometer {
public:
    /**
     * Default multiplier to convert from raw potentiometer output to angles.
     *
     * Potentiometers have a (nominal) range of 250 degrees and output 12-bit
     * values, so the default scaling is 25 / 4095.
     */
    static constexpr QAngle DEFAULT_MULTIPLIER = 265.0 / 4095.0 * degree;

    /**
     * Construct a new potentiometer.
     *
     * @param port Port to run on. This can be 1-8, 'a'-'h', or 'A'-'H'.
     * @param reversed Whether the potentiometer should be reversed.
     * @param multiplier Multiplier to convert form raw potentiometer output
     * into an angle.
     * @param offset Offset used to report values relative to another point.
     */
    Pot(std::uint8_t port,
        bool reversed = false,
        QAngle multiplier = DEFAULT_MULTIPLIER,
        QAngle offset = 0.0 * degree);

    Pot(const Pot&) = delete;
    Pot operator=(const Pot&) = delete;

    Pot(Pot&& other) = default;

    /**
     * Returns the current potentiometer position as an angle.
     *
     * @return Current position, relative to offset.
     */
    QAngle get_angle() const;

protected:
    /**
     * Convert a raw potentiometer output into an angle, shifted by offset.
     */
    QAngle convert_raw(std::int32_t raw) const;

    /**
     * Whether the potentiometer should be reversed.
     */
    bool reversed;

    /**
     * Multiplier to convert from raw potentiometer output to an angle.
     */
    const QAngle multiplier;

    /**
     * Offset used when reporting angles.
     */
    const QAngle offset;
};

/**
 * Calibrated potentiometer.
 *
 * This should be used when either there is a fixed starting position or only
 * relative movement matters. In these cases, it can give a more precise reading
 * by reducing noise.
 */
class CalibratedPot : protected Pot {
public:
    /**
     * Constructs a CalibratedPot object.
     *
     * NOTE: This calibrates the potentiometer to its current position, which
     * takes about 0.5 seconds and during which time the potentiometer should be
     * completely still.
     *
     * @param port Port to run on. This can be 1-8, 'a'-'h', or 'A'-'H'.
     * @param reversed Whether the potentiometer should be reversed.
     * @param multiplier Multiplier to convert form raw potentiometer output
     * into an angle.
     * @param offset Offset used to report values relative to another point.
     * Note that this offset is relative to the initial position of the
     * potentiometer when this is called.
     */
    CalibratedPot(
            std::uint8_t port,
            bool reversed,
            QAngle multiplier = DEFAULT_MULTIPLIER,
            QAngle offset = 0.0 * degree);

    CalibratedPot(const CalibratedPot&) = delete;
    CalibratedPot& operator=(const CalibratedPot&) = delete;

    CalibratedPot(CalibratedPot&& other) = default;

    /**
     * Returns the current potentiometer position as an angle.
     *
     * @return Current position, relative to offset.
     */
    QAngle get_angle() const;
};

} // namespace purpl

#endif // PURPL_POTENTIOMETER_HPP_
