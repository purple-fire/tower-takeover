#include "common/program_selection.hpp"
#include "main.h"
#include "menu.hpp"
#include "purpl/math.hpp"
#include "robot.hpp"

using Drive = std::decay_t<decltype(current_robot().get_drive_train())>;
using TranslationConfig = Drive::TranslationControl::Config;
using RotationConfig = Drive::RotationControl::Config;

static TranslationConfig fast_move_config{
        {3.3_Hz, 0.75_Hz * 1.0_Hz, 0.1_num},
        1_cm,
        4_mps,
        8_mps2,
};

static TranslationConfig const_move_config{
        {100_Hz, 0_Hz * 1_Hz, 0_num},
        5_cm,
        4_mps,
        100_mps2,
};

static RotationConfig fast_rotate_config{
        {3.0_Hz, 0.07_Hz * 1.0_Hz, 0.1_num},
        1_deg,
        185_dps,
        450_dps / 1_s,
};

static TranslationConfig cube_move_config{
        {3.3_Hz, 0.1_Hz * 1.0_Hz, 0.05_num},
        1_cm,
        0.7_mps,
        2.6_mps2,
};

static TranslationConfig slow_move_config{
        {3.3_Hz, 0.1_Hz * 1.0_Hz, 0.05_num},
        1_cm,
        1.3_mps,
        2.6_mps2,
};

static TranslationConfig pick_up_move_config{
        {3.3_Hz, 0.75_Hz * 1.0_Hz, 0.05_num},
        1_cm,
        0.2_mps,
        6_mps2,
};

static RotationConfig slow_rotate_config{
        {3.0_Hz, 0.005_Hz * 1.0_Hz, 0.01_num},
        1_deg,
        100_dps,
        300_dps / 1_s,
};

class Autonomous {
public:
    Autonomous(Robot& robot) :
        robot(robot),
        drive_train(robot.get_drive_train()),
        recliner(robot.get_recliner()),
        intake(robot.get_intake()),
        arms(robot.get_arms()) {}
    ReclinerState recliner_state = ReclinerState::backward;

    Robot& robot;
    Robot::Drive& drive_train;
    Robot::Recliner& recliner;
    Robot::IntakeSystem& intake;
    Robot::Arms& arms;

    void run() {
        controller_menu.start_task();

        drive_train.start_task();
        recliner.start_task();
        intake.start_task();

        // Skills routine puts cubes in 3 towers and picks up a few cubes
        // to drop off in the scoring zone
        // TODO Complete the last part
        if (PROGRAM == Program::skills) {
            // Move the preload out of the way a bit so we can unlock without
            // hitting it.

            pick_up(9_in);
            wait_move_settled();

            pick_up(-8_in);
            wait_move_settled();

            unlock();

            // The arms move a lot, so put the recliner in position before
            // everything
            recliner.set_state(ReclinerState::free_arms);

            //
            // First tower
            //

            // Start moving forward
            move_slow(19_in);

            drive_train.wait_move_settled(14_in, 0_s);
            intake.intake();

            drive_train.wait_move_settled(5_in, 0_s);
            intake.stop();

            // Move the arms up on the way to the tower
            arms.set_target(ARMS_LOW_TOWER);
            pros::delay(1000);
            wait_move_settled();

            // Put the cube in the tower
            intake.outtake();
            pros::delay(1000);
            intake.stop();

            // Move back to the wall and put the arms down
            // move_slow(-24_in);
            drive_train.move_internal(-26_in, -21_in, 0.5_mps);
            arms.set_target(ARMS_BOTTOM_MAX);
            wait_move_settled();

            // rotate_slow(-45_deg);
            // drive_train.wait_rotation_settled(5_deg);
            // slam_wall();
            slam_wall();

            //
            // Second tower
            //

            // Move to the next tower
            move_slow(46_in);

            // Outtake to help push the line of cubes out of the way
            intake.outtake();

            drive_train.wait_move_settled(16_in, 0_s);
            intake.stop();

            // Turn the intake on a few inches before we ge there to pick up the
            // cube at the base of the tower
            drive_train.wait_move_settled(8_in, 0_s);
            intake.intake();
            pros::delay(500);
            intake.stop();
            wait_move_settled();

            // Back up a bit to lift the arms
            arms.set_target(ARMS_LOW_TOWER);
            pros::delay(2000);
            move_slow(3_in);
            drive_train.wait_move_settled();

            // Put the cube in the tower
            intake.outtake();
            pros::delay(1000);
            intake.stop();

            arms.set_target(ARMS_BOTTOM_MAX);
            move_slow(-29_in);
            wait_move_settled();

            //
            // Third tower
            //

            // TODO Slam the wall again?

            // Face the 3rd tower
            rotate_slow(-89_deg);
            wait_rotation_settled();

            // Move up to the tower
            move_slow(17_in);

            // Turn the intake on a few inches before we ge there to pick up the
            // cube at the base of the tower
            drive_train.wait_move_settled(8_in, 0_s);
            intake.intake();
            pros::delay(500);
            intake.stop();
            wait_move_settled();

            // Lift the arms and move foward a bit to the tower
            arms.set_target(ARMS_MID_TOWER);
            pros::delay(2000);
            move_slow(4_in);
            wait_move_settled();

            // Put the cube in the tower
            intake.outtake();
            pros::delay(1000);
            intake.stop();

            // Back up to the line of cubes
            move_slow(-8_in);
            wait_move_settled();

            // Let the arms down
            arms.set_target(ARMS_BOTTOM_MAX);

            /*

            //
            // Pick up first row of cubes
            //

            // Turn to face it
            rotate_slow(0_deg);

            // Pick it up
            intake.intake();
            pick_up(16_in);
            wait_move_settled();
            // pros::delay(500);
            intake.stop();

            // Back up and slam against the wall
            move_slow(-36_in);
            wait_move_settled();
            slam_wall();

            */
        } else {
            // Just unlock and back up a few inches to push the preload into the
            // scoring zone
            // TODO Make an actual routine which stacks a few cubes

            unlock();
            move_slow(-8_in);
            wait_move_settled();

            // Wait a bit to fall off of the ramp
            pros::delay(1000);

            // Rotate away from the scoring zone
            rotate_slow(45_deg);
            wait_rotation_settled();

            // Move away from the zone and the wall
            move_slow(8_in);
            wait_move_settled();

            // Rotate back towards the cube in the zone so we can pick it up
            // next
            rotate_slow(160_deg);
            wait_rotation_settled();
        }
    }

private:
    void destack() {
        intake.stop();
        recliner.set_state(ReclinerState::forward);
        recliner.wait_settled(3_deg);
    }

    void unlock() {
        recliner.set_state(ReclinerState::free_arms);
        arms.set_target(ARMS_BOTTOM_MAX);
        pros::delay(1000);
        intake.outtake();
        pros::delay(500);
        intake.stop();
        arms.set_target(ARMS_LOW_TOWER);
        pros::delay(1500);
        arms.set_target(ARMS_BOTTOM_MAX);
        pros::delay(1000);
    }

    void drive_goal() {
        drive_train.user_control();
        drive_train.move_velocity(0.3 * drive_train.get_max_speed());
        pros::delay(600);
        drive_train.move_velocity(0_mps);
        pros::delay(600);
    }

    void slam_wall() {
        drive_train.user_control();
        drive_train.move_velocity(-0.25 * drive_train.get_max_speed());
        pros::delay(750);
        drive_train.move_velocity(0_mps);
        pros::delay(750);
        drive_train.tare_gyro();
    }

    void slam_wall_left() {
        drive_train.user_control();
        drive_train.move_velocity(
                -0.5 * drive_train.get_max_speed(),
                -0.1 * drive_train.get_max_speed());
        pros::delay(750);
        drive_train.move_velocity(0_mps);
        pros::delay(750);
        drive_train.tare_gyro();
    }

    void move_slow(purpl::QLength dist) {
        // drive_train.move(slow_move_config, dist);
        drive_train.move_internal(dist, 0.5_mps);
    }

    void move_cube(purpl::QLength dist) {
        // drive_train.move(cube_move_config, dist);
        drive_train.move_internal(dist, 0.5_mps);
    }

    void pick_up(purpl::QLength dist) {
        // drive_train.move(pick_up_move_config, dist);
        drive_train.move_internal(dist, 0.2_mps);
    }

    void move_fast(purpl::QLength dist) {
        // drive_train.move(fast_move_config, dist);
        drive_train.move_internal(dist, 2.0_mps);
    }

    void const_move(purpl::QLength dist) {
        // drive_train.move(const_move_config, dist);
        drive_train.move_internal(dist, 2.0_mps);
    }

    void rotate_slow(purpl::QAngle angle) {
        drive_train.rotate(slow_rotate_config, angle);
        // drive_train.rotate_internal(angle);
    }

    void rotate_fast(purpl::QAngle angle) {
        drive_train.rotate(fast_rotate_config, angle);
        // drive_train.rotate_internal(angle);
    }

    void wait_move_settled() {
        drive_train.wait_move_settled();
    }

    void wait_rotation_settled() {
        drive_train.wait_rotation_settled(2_deg);
    }

    purpl::QAngle bottom = ARMS_BOTTOM_MAX;
    purpl::QAngle low_tower = ARMS_LOW_TOWER;
    purpl::QAngle top = ARMS_TOP_MAX;
};

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {
    Autonomous more_auto(current_robot());
    more_auto.run();
}
