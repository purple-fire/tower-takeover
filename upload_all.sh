#!/bin/sh

if [ -z "$1" ]; then
    echo "usage: $0 ROBOT_NAME"
    exit 1
fi

base_name="$1"

# Fail if any of the builds/uploads fail
set -e

cd "$base_name"

upload_program() {
    make PROGRAM="$1"
    prosv5 upload --name "${base_name}-$2" --slot $3
}

# Clear all programs and build artifacts
make clean
prosv5 v5 rm-all

upload_program RED red 1
upload_program BLUE blue 2
upload_program SKILLS skills 3
