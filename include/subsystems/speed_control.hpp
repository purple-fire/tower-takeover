#ifndef SPEED_CONTROL_HPP_
#define SPEED_CONTROL_HPP_

#include "purpl/accel_limit_control.hpp"
#include "purpl/pid_control.hpp"
#include "purpl/units.hpp"

template <typename Time, typename Input>
class SpeedController {
public:
    using Velocity = decltype(std::declval<Input>() / std::declval<Time>());
    using Accel = decltype(std::declval<Velocity>() / std::declval<Time>());

    using PIDController = purpl::PIDController<Time, Input>;
    using Gains = typename PIDController::Gains;

    using AccelController = purpl::AccelLimitController<Time, Velocity>;

    struct Config {
        Gains gains;
        Input tol;
        Velocity max_vel;
        Accel max_accel;
    };

    SpeedController() {}

    SpeedController(Gains gains, Input tol, Velocity max_vel, Accel max_accel) :
        pid_control(gains),
        tolerance(tol),
        max_velocity(max_vel),
        accel_control(max_accel) {}

    SpeedController(Config config) :
        SpeedController(
                config.gains, config.tol, config.max_vel, config.max_accel) {}

    Input get_target() const {
        return pid_control.get_target();
    }

    void set_target(Input target) {
        pid_control.set_target(target);
    }

    Input get_error() const {
        return pid_control.get_error();
    }

    Velocity step(Time delta, Input pos) {
        auto output = pid_control.step(delta, pos);
        output = purpl::copysign(
                std::min(purpl::abs(output), max_velocity), output);
        output = accel_control.step(delta, output);

        // Stop once within tolerance.
        // We still run the pid and acceleration controllers so that they can
        // resume operation if it moves out of tolerance again.
        if (purpl::abs(get_error()) < tolerance) {
            return Velocity();
        } else {
            return output;
        }
    }

private:
    PIDController pid_control;

    Input tolerance;
    Velocity max_velocity;
    AccelController accel_control;
};

#endif // SPEED_CONTROL_HPP_
