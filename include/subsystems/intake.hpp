#pragma once
#ifndef INTAKE_HPP_
#define INTAKE_HPP_

#include "purpl/async_wrapper.hpp"
#include "purpl/math.hpp"
#include "purpl/motor.hpp"
#include "purpl/units.hpp"
#include "purpl/utils.hpp"

#include <variant>
using namespace purpl::literals;

template <class M>
class Intake : public purpl::AsyncWrapper<Intake<M>> {
public:
    const purpl::QAngularVelocity high_velocity;

    Intake(M&& motors,
           purpl::QAngularVelocity high_speed,
           purpl::QAngularVelocity low_speed,
           purpl::QLength roller_radius,
           pros::ADIAnalogIn bottom_line_tracker,
           pros::ADIAnalogIn top_line_tracker,
           std::uint16_t intensity_cutoff) :
        purpl::AsyncWrapper<Intake<M>>(this),
        motors(std::move(motors)),
        high_speed{purpl::abs(high_speed)},
        low_speed{purpl::abs(low_speed)},
        roller_radius{roller_radius},
        bottom_line_tracker{bottom_line_tracker},
        top_line_tracker{top_line_tracker},
        intensity_cutoff{intensity_cutoff} {}

    Intake(const Intake&) = delete;
    Intake& operator=(const Intake& other) = delete;

    void set_velocity(purpl::QAngularVelocity vel) {
        state = IntakeVel{};
        current_vel = vel;
    }

    void set_linear_velocity(purpl::QVelocity vel) {
        set_velocity(vel_to_angular(vel));
    }

    void intake() {
        set_velocity(high_speed);
    }

    void outtake() {
        set_velocity(-high_speed);
    }

    void stop() {
        set_velocity(0 * purpl::rpm);
    }

    void intake_single_cube(purpl::QAngularVelocity speed) {
        state = IntakeSingle{false};
        current_vel = speed;
    }

    void intake_single_cube() {
        intake_single_cube(high_speed);
    }

    void outtake_to_bottom(purpl::QAngularVelocity speed) {
        state = OuttakeBottom{};
        current_vel = speed;
    }

    void outtake_to_bottom() {
        outtake_to_bottom(low_speed);
    }

    bool cube_at_bottom() const {
        return bottom_line_tracker.get_value() < intensity_cutoff;
    }

    bool cube_at_top() const {
        return top_line_tracker.get_value() < intensity_cutoff;
    }

    void loop() {
        purpl::make_async_loop(LOOP_DELAY, [&](std::uint32_t delay_millis) {
            const auto delay = delay_millis * purpl::millisecond;

            const auto handler = purpl::Visitor{
                    [&](IntakeVel& state) {
                        motors.move_velocity(current_vel);
                    },
                    [&](IntakeSingle& single_state) {
                        // Check if the intake has took in a cube yet
                        if (!single_state.had_cube && cube_at_bottom()) {
                            single_state.had_cube = true;
                        }

                        // Run the intake until the cube reaches the top line
                        // tracker
                        if (single_state.had_cube && cube_at_top()) {
                            // The cube has been fully intooken
                            // Just set the mode back to IntakeVel because it
                            // sometimes starts jumbling the cube otherwise
                            set_velocity(0 * purpl::rpm);
                        } else {
                            motors.move_velocity(current_vel);
                        }
                    },
                    [&](OuttakeBottom& state) {
                        // We want to move the stack until only the top one is
                        // covered
                        if (cube_at_bottom()) {
                            motors.move_velocity(current_vel);
                        } else if (!cube_at_top()) {
                            motors.move_velocity(-current_vel);
                        } else {
                            set_velocity(0 * purpl::rpm);
                        }
                    },
            };

            std::visit(handler, state);
        });
    }

private:
    static constexpr std::uint32_t LOOP_DELAY = 20;

    purpl::QAngularVelocity vel_to_angular(purpl::QVelocity vel) const {
        return vel / roller_radius * purpl::radian;
    }

    M motors;

    const purpl::QAngularVelocity high_speed;
    const purpl::QAngularVelocity low_speed;

    const purpl::QLength roller_radius;

    pros::ADIAnalogIn bottom_line_tracker;
    pros::ADIAnalogIn top_line_tracker;

    const uint16_t intensity_cutoff;

    struct IntakeVel {};

    struct IntakeSingle {
        bool had_cube;
    };

    struct OuttakeBottom {};

    using IntakeState = std::variant<IntakeVel, IntakeSingle, OuttakeBottom>;
    purpl::QAngularVelocity current_vel = 0 * purpl::rpm;

    IntakeState state{IntakeVel{}};
};

#endif // INTAKE_HPP_
