/**
 * Group of connected motors.
 *
 * @copyright Copyright (c) 2019 Purple Fire Robotics
 *
 * This uses the C API because the C++ motor API uses virtual functions for no
 * reason.
 *
 * TODO Remove the non-dimensioned method overloads?
 */

#pragma once

#ifndef PURPL_MOTOR_HPP_
#define PURPL_MOTOR_HPP_

#include "api.h"
#include "units.hpp"

namespace purpl {

enum class BrakeMode {
    brake = pros::E_MOTOR_BRAKE_BRAKE,
    coast = pros::E_MOTOR_BRAKE_COAST,
    hold = pros::E_MOTOR_BRAKE_HOLD,
};

enum class Gearset {
    red = pros::E_MOTOR_GEARSET_36,
    green = pros::E_MOTOR_GEARSET_18,
    blue = pros::E_MOTOR_GEARSET_06,
};

enum class EncoderUnits {
    degrees = pros::E_MOTOR_ENCODER_DEGREES,
    counts = pros::E_MOTOR_ENCODER_COUNTS,
    rotations = pros::E_MOTOR_ENCODER_ROTATIONS,
};

struct Gearing {
    Gearset internal;
    double external;
};

/**
 * Structure containing per-motor configuration for motors in a motor group.
 */
struct MotorConfig {
    /**
     * Construct a motor configuration from a port.
     *
     * @param port Port of the motor. A negative port indicates that the motor
     * should be reversed (with the port value being the absolute value of the
     * port passed).
     */
    MotorConfig(std::int8_t port) :
        port(port < 0 ? -port : port), reversed(port < 0) {}

    /**
     * Construct a motor configuration, optionally reversing the motor.
     *
     * @param port Port of the motor.
     * @param reversed Whether the motor is reversed.
     */
    MotorConfig(std::uint8_t port, bool reversed) :
        port(port), reversed(reversed) {}

    std::uint8_t port;
    bool reversed;

    void apply_config() {
        pros::c::motor_set_reversed(port, reversed);
    }
};

template <size_t N>
class MotorGroup {
public:
    static_assert(N > 0, "MotorGroup must have at least 1 motor");

    /**
     * Construct a MotorGroup from a set of motors.
     *
     * This will tare the position of all motors so that they can be kept in
     * sync.
     *
     * This can be called using implicit constructors of MotorConfig like so:
     *   MotorGroup(1, 2, -3)
     * which will create a group with motors at ports 1, 2, and 3, with motor
     * 3 reversed.
     *
     * @param configs Configurations for motors in the group. These should be of
     * type MotorConfig or be able to be converted into MotorConfig objects.
     */
    template <typename... Ms>
    MotorGroup(Ms... configs) : MotorGroup(1.0, configs...) {}

    /**
     * Construct a MotorGroup with external gearing.
     *
     * This will tare the position of all motors so that they can be kept in
     * sync.
     *
     * This can be called using implicit constructors of MotorConfig like so:
     *   MotorGroup(1, 2, -3)
     * which will create a group with motors at ports 1, 2, and 3, with motor
     * 3 reversed.
     *
     * @param extern_gearing External gearing of the motors. This value is the
     * number of output rotations per rotation of a motor. It is assumed all
     * motors are geared together evenly.
     * @param configs Configurations for motors in the group. These should be of
     * type MotorConfig or be able to be converted into MotorConfig objects.
     */
    template <typename... Ms>
    MotorGroup(double extern_gearing, Ms... configs) :
        MotorGroup(
                {Gearset::green, extern_gearing},
                EncoderUnits::degrees,
                BrakeMode::coast,
                configs...) {}

    /**
     * Construct a MotorGroup with external gearing.
     *
     * This will tare the position of all motors so that they can be kept in
     * sync.
     *
     * This can be called using implicit constructors of MotorConfig like so:
     *   MotorGroup(1, 2, -3)
     * which will create a group with motors at ports 1, 2, and 3, with motor
     * 3 reversed.
     *
     * @param brake_mode Brake mode to be set for all motors.
     * @param configs Configurations for motors in the group. These should be of
     * type MotorConfig or be able to be converted into MotorConfig objects.
     */
    template <typename... Ms>
    MotorGroup(BrakeMode brake_mode, Ms... configs) :
        MotorGroup(1.0, brake_mode, configs...) {}

    /**
     * Construct a MotorGroup with external gearing.
     *
     * This will tare the position of all motors so that they can be kept in
     * sync.
     *
     * This can be called using implicit constructors of MotorConfig like so:
     *   MotorGroup(1, 2, -3)
     * which will create a group with motors at ports 1, 2, and 3, with motor
     * 3 reversed.
     *
     * @param extern_gearing External gearing of the motors. This value is the
     * number of output rotations per rotation of a motor. It is assumed all
     * motors are geared together evenly.
     * @param brake_mode Brake mode to be set for all motors.
     * @param configs Configurations for motors in the group. These should be of
     * type MotorConfig or be able to be converted into MotorConfig objects.
     */
    template <typename... Ms>
    MotorGroup(double extern_gearing, BrakeMode brake_mode, Ms... configs) :
        MotorGroup({Gearset::green, extern_gearing}, brake_mode, configs...) {}

    /**
     * Construct a MotorGroup with external gearing.
     *
     * This will tare the position of all motors so that they can be kept in
     * sync.
     *
     * This can be called using implicit constructors of MotorConfig like so:
     *   MotorGroup(1, 2, -3)
     * which will create a group with motors at ports 1, 2, and 3, with motor
     * 3 reversed.
     *
     * @param gearing Internal and external gearing of the motors. All motors
     * will be set to the same internal gearing.
     * @param brake_mode Brake mode to be set for all motors.
     * @param configs Configurations for motors in the group. These should be of
     * type MotorConfig or be able to be converted into MotorConfig objects.
     */
    template <typename... Ms>
    MotorGroup(Gearing gearing, BrakeMode brake_mode, Ms... configs) :
        MotorGroup(gearing, EncoderUnits::degrees, brake_mode, configs...) {}

    /**
     * Construct a MotorGroup with external gearing.
     *
     * This will tare the position of all motors so that they can be kept in
     * sync.
     *
     * This can be called using implicit constructors of MotorConfig like so:
     *   MotorGroup(1, 2, -3)
     * which will create a group with motors at ports 1, 2, and 3, with motor
     * 3 reversed.
     *
     * @param gearing Internal and external gearing of the motors. All motors
     * will be set to the same internal gearing.
     * @param encoder_units Encoder units to be set for all motors.
     * @param brake_mode Brake mode to be set for all motors.
     * @param configs Configurations for motors in the group. These should be of
     * type MotorConfig or be able to be converted into MotorConfig objects.
     */
    template <typename... Ms>
    MotorGroup(
            Gearing gearing,
            EncoderUnits encoder_units,
            BrakeMode brake_mode,
            Ms... configs) :
        gearing(gearing.external), ports{MotorConfig(configs).port...} {
        static_assert(
                sizeof...(Ms) == N,
                "MotorGroup must be constructed with all of its motors");
        for (const auto port : ports) {
            pros::c::motor_set_gearing(
                    port,
                    static_cast<pros::motor_gearset_e_t>(gearing.internal));
            pros::c::motor_set_encoder_units(
                    port,
                    static_cast<pros::motor_encoder_units_e_t>(encoder_units));
            pros::c::motor_set_brake_mode(
                    port, static_cast<pros::motor_brake_mode_e_t>(brake_mode));
        }

        (MotorConfig(configs).apply_config(), ...);

        // Make sure all the motors have the same 0 position
        tare_position();
    }

    // Motors should not be referenced from multiple objcets, so the copy
    // constructor/assignment operator are deleted.

    MotorGroup(const MotorGroup<N>&) = delete;
    MotorGroup& operator=(const MotorGroup<N>&) = delete;

    MotorGroup(MotorGroup<N>&& other) : gearing(other.gearing) {
        // Arrays con't be constructed in the initializer list
        for (std::size_t i = 0; i < N; ++i) {
            ports[i] = other.ports[i];
        }
    }

    std::int32_t move_voltage(std::int16_t voltage) const {
        return apply_all([=](auto port) {
            return pros::c::motor_move_voltage(port, voltage);
        });
    }

    std::int32_t move_absolute(double position, std::int32_t velocity) const {
        const double adj_pos = position / gearing;
        const std::int32_t adj_vel = velocity / gearing;
        return apply_all([=](auto port) {
            return pros::c::motor_move_absolute(port, adj_pos, adj_vel);
        });
    }

    std::int32_t
    move_absolute(QAngle position, QAngularVelocity velocity) const {
        const auto units = get_encoder_units();
        if (units == purpl::QAngle()) {
            return PROS_ERR;
        }

        return move_absolute(position.convert(units), velocity.convert(rpm));
    }

    std::int32_t move_absolute(QAngle position) const {
        const auto max_speed = get_max_speed();
        if (max_speed == purpl::QAngularVelocity()) {
            return PROS_ERR;
        }

        return move_absolute(position, max_speed);
    }

    std::int32_t move_relative(double position, std::int32_t velocity) const {
        const double adj_pos = position / gearing;
        const std::int32_t adj_vel = velocity / gearing;
        return apply_all([=](auto port) {
            return pros::c::motor_move_relative(port, adj_pos, adj_vel);
        });
    }

    std::int32_t
    move_relative(QAngle position, QAngularVelocity velocity) const {
        const auto units = get_encoder_units();
        if (units == purpl::QAngle()) {
            return PROS_ERR;
        }

        return move_relative(position.convert(units), velocity.convert(rpm));
    }

    std::int32_t move_relative(QAngle position) const {
        const auto max_speed = get_max_speed();
        if (max_speed == purpl::QAngularVelocity()) {
            return PROS_ERR;
        }

        return move_relative(position, max_speed);
    }

    std::int32_t move_velocity(std::int16_t velocity) const {
        const std::int16_t adj_vel = velocity / gearing;
        return apply_all([=](auto port) {
            return pros::c::motor_move_velocity(port, adj_vel);
        });
    }

    std::int32_t move_velocity(QAngularVelocity velocity) const {
        return move_velocity(velocity.convert(rpm));
    }

    QAngularVelocity get_velocity() const {
        // Velocities are always in RPN
        return pros::c::motor_get_actual_velocity(ports[0]) * purpl::rpm;
    }

    QAngle get_position() const {
        return pros::c::motor_get_position(ports[0]) * gearing *
               get_encoder_units();
    }

    QAngle get_target_position() const {
        return pros::c::motor_get_target_position(ports[0]) * gearing *
               get_encoder_units();
    }

    std::int32_t get_direction() const {
        return pros::c::motor_get_direction(ports[0]);
    }

    // TODO Average values from all motors for these?

    std::int32_t get_torque() const {
        return pros::c::motor_get_torque(ports[0]);
    }

    double get_power() const {
        return sum_all(
                [](auto port) { return pros::c::motor_get_power(port); });
    }

    double get_voltage() const {
        return pros::c::motor_get_voltage(ports[0]);
    }

    std::int32_t get_current_draw() {
        return sum_all([](auto port) {
            return pros::c::motor_get_current_draw(port);
        });
    }

    double get_efficiency() const {
        return pros::c::motor_get_efficiency(ports[0]);
    }

    double get_temperature() const {
        return pros::c::motor_get_temperature(ports[0]);
    }

    std::uint32_t get_faults() const {
        std::int32_t res = 0;
        for (const auto port : ports) {
            res |= pros::c::motor_get_faults(port);
        }

        return res;
    }

    std::uint32_t get_flags() const {
        std::int32_t res = 0;
        for (const auto port : ports) {
            res |= pros::c::motor_get_flags(port);
        }

        return res;
    }

    std::int32_t get_zero_position_flag() const {
        // All motors must be in zero position
        for (const auto port : ports) {
            auto retval = pros::c::motor_get_zero_position_flag(port);
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (!retval) {
                return false;
            }
        }

        return true;
    }

    std::int32_t is_stopped() const {
        // All motors must be stopped
        for (const auto port : ports) {
            auto retval = pros::c::motor_is_stopped(port);
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (!retval) {
                return false;
            }
        }

        return true;
    }

    std::int32_t is_over_current() const {
        // Any motor can be over current
        for (const auto port : ports) {
            auto retval = pros::c::motor_is_over_current(port);
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (retval) {
                return true;
            }
        }

        return false;
    }

    std::int32_t is_over_temp() const {
        // Any motor can be over temp
        for (const auto port : ports) {
            auto retval = pros::c::motor_is_over_temp(port);
            if (retval == PROS_ERR) {
                return PROS_ERR;
            } else if (retval) {
                return true;
            }
        }

        return false;
    }

    BrakeMode get_brake_mode() const {
        return static_cast<BrakeMode>(pros::c::motor_get_brake_mode(ports[0]));
    }

    Gearset get_gearset() const {
        return static_cast<Gearset>(pros::c::motor_get_gearing(ports[0]));
    }

    QAngularVelocity get_gearset_speed() const {
        switch (get_gearset()) {
        case Gearset::red:
            return 100 * rpm;
        case Gearset::green:
            return 200 * rpm;
        case Gearset::blue:
            return 600 * rpm;
        default:
            return purpl::QAngularVelocity();
        }
    }

    QAngularVelocity get_max_speed() const {
        return gearing * get_gearset_speed();
    }

    QAngle get_encoder_units() const {
        // TODO Check that all motors are the same?
        switch (pros::c::motor_get_encoder_units(ports[0])) {
        case pros::E_MOTOR_ENCODER_DEGREES:
            return degree;
        case pros::E_MOTOR_ENCODER_COUNTS:
            return degree / 10;
        case pros::E_MOTOR_ENCODER_ROTATIONS:
            return 360 * degree;
        default:
            return purpl::QAngle();
        }
    }

    std::int32_t is_reversed() const {
        return pros::c::motor_is_reversed(ports[0]);
    }

    std::int32_t get_voltage_limit() const {
        // TODO Get minimum?
        return pros::c::motor_get_voltage_limit(ports[0]);
    }

    std::int32_t get_current_limit() const {
        // TODO Get minimum?
        return pros::c::motor_get_current_limit(ports[0]);
    }

    std::int32_t set_brake_mode(BrakeMode mode) const {
        std::int32_t res = 1;
        auto pros_mode = static_cast<pros::motor_brake_mode_e_t>(mode);
        for (const auto port : ports) {
            if (pros::c::motor_set_brake_mode(port, pros_mode) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    std::int32_t set_gearing(Gearset set) const {
        std::int32_t res = 1;
        const auto pros_set = static_cast<pros::motor_gearset_e_t>(set);
        for (const auto port : ports) {
            if (pros::c::motor_set_gearing(port, pros_set) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    std::int32_t set_encoder_units(EncoderUnits units) const {
        std::int32_t res = 1;
        const auto pros_units =
                static_cast<pros::motor_encoder_units_e_t>(units);
        for (const auto port : ports) {
            if (pros::c::motor_set_encoder_units(port, pros_units) ==
                PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    std::int32_t set_voltage_limit(std::int32_t limit) const {
        std::int32_t res = 1;
        for (const auto port : ports) {
            if (pros::c::motor_set_voltage_limit(port, limit) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    std::int32_t set_current_limit(std::int32_t limit) const {
        std::int32_t res = 1;
        for (const auto port : ports) {
            if (pros::c::motor_set_current_limit(port, limit) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    std::int32_t set_zero_position(double position) const {
        const double adj_pos = position / gearing;

        std::int32_t res = 1;
        for (const auto port : ports) {
            if (pros::c::motor_set_zero_position(port, adj_pos) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    std::int32_t set_zero_position(purpl::QAngle position) const {
        return set_zero_position(position.convert(get_encoder_units()));
    }

    std::int32_t tare_position() const {
        std::int32_t res = 1;
        for (const auto port : ports) {
            if (pros::c::motor_tare_position(port) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

private:
    template <typename F>
    std::int32_t apply_all(F f) const {
        std::int32_t res = 1;
        for (const auto port : ports) {
            if (f(port) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    template <typename T, typename F>
    T sum_all(F f) const {
        T total;
        for (const auto port : ports) {
            total += f(port);
        }

        return total / N;
    }

    /**
     * External gearing of the motor group
     */
    double gearing;

    /**
     * Ports of the motors in the group.
     */
    std::uint8_t ports[N];
};

using Motor = MotorGroup<1>;

/**
 * Class for a group of motors which are not geared together evenly.
 */
template <size_t N>
class UnevenMotorGroup {
public:
    template <typename... Ms>
    UnevenMotorGroup(Ms&&... motors) : motors{std::move(motors)...} {}

    std::int32_t move_absolute(purpl::QAngle angle) const {
        return apply_all(
                [=](const auto& motor) { return motor.move_absolute(angle); });
    }

    std::int32_t
    move_absolute(purpl::QAngle angle, purpl::QAngularVelocity vel) const {
        return apply_all([=](const auto& motor) {
            return motor.move_absolute(angle, vel);
        });
    }

    std::int32_t move_relative(purpl::QAngle angle) const {
        return apply_all(
                [=](const auto& motor) { return motor.move_relative(angle); });
    }

    std::int32_t
    move_relative(purpl::QAngle angle, purpl::QAngularVelocity vel) const {
        return apply_all([=](const auto& motor) {
            return motor.move_relative(angle, vel);
        });
    }

    std::int32_t move_velocity(purpl::QAngularVelocity vel) const {
        return apply_all(
                [=](const auto& motor) { return motor.move_velocity(vel); });
    }

    purpl::QAngularVelocity get_max_velocity() const {
        // Get minimum value from all the motors
        purpl::QAngularVelocity max_vel = motors[0].get_max_velocity();
        for (std::size_t i = 1; i < N; ++i) {
            if (motors[i].get_max_velocity() < max_vel) {
                max_vel = motors[i].get_max_velocity();
            }
        }

        return max_vel;
    }

private:
    template <typename F>
    std::int32_t apply_all(F f) const {
        std::int32_t res = 1;
        for (const auto& motor : motors) {
            if (f(motor) == PROS_ERR) {
                res = PROS_ERR;
            }
        }

        return res;
    }

    // MotorGroup<1> encodes the external gearing for each one.
    Motor motors[N];
};

} // namespace purpl

#endif // PURPL_MOTOR_HPP_
