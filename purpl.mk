PROJECT_ROOT:=..
EXTRA_INCDIR+=$(PROJECT_ROOT)/include

# If a program is specified rebuild anything that uses it
ifneq ($(PROGRAM),)
# Hack to re-build anything that depends on the header
.PHONY: $(PROJECT_ROOT)/include/common/program_selection.hpp
endif

# Default the program to skills
PROGRAM?=SKILLS
# Define the PROG_* variable to specify the autonomous program
PROGRAM_DEFINE:=-DPROG_$(PROGRAM)
EXTRA_CFLAGS+=$(PROGRAM_DEFINE)
EXTRA_CXXFLAGS+=$(PROGRAM_DEFINE)
