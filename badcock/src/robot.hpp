
#pragma once

#ifndef ROBOT_HPP_
#define ROBOT_HPP_

#include "main.h"
#include "purpl/motor.hpp"
#include "purpl/potentiometer.hpp"
#include "subsystems/drive_controller.hpp"
#include "subsystems/intake.hpp"
#include "subsystems/intake_arms.hpp"
#include "subsystems/recliner_async.hpp"

constexpr purpl::QAngle ARMS_BOTTOM_MAX = 83_deg;
constexpr purpl::QAngle ARMS_LOW_TOWER = 145_deg;
// constexpr purpl::QAngle ARMS_MID_TOWER = Nope;
// constexpr purpl::QAngle ARMS_HIGH_TOWER = Nope;
constexpr purpl::QAngle ARMS_TOP_MAX = 150_deg;

/**
 * Singleton class for the whole robot configuration.
 */
class Robot {
public:
    // Delete copy constructors to help avoid referencing the robot from
    // multiple places

    Robot(const Robot&) = delete;
    Robot& operator=(const Robot& other) = delete;

    using Drive = DriveController<purpl::MotorGroup<3>>;
    using Arms = IntakeArms<purpl::Motor>;
    using IntakeSystem = Intake<purpl::UnevenMotorGroup<4>>;
    using Recliner = ReclinerAsyncController<purpl::MotorGroup<2>>;

    Drive& get_drive_train();

    Recliner& get_recliner();

    IntakeSystem& get_intake();

    Arms& get_arms();

private:
    /**
     * Constructs the robot with pre-defined ports.
     *
     * This is private to prevent making multiple instances of this.
     */
    Robot();

    friend void initialize_robot();

    Drive drive_train;

    IntakeSystem intake;

    Arms arms;

    Recliner recliner;
};

/**
 * Initialize the global robot instance.
 *
 * This should be called exactly once, from initialize().
 */
void initialize_robot();

/**
 * Reference to the current active robot.
 *
 * This is dynamlically-allocated so that it can be constructed from
 * initalize() while still being accessible to other parts of the code.
 */
Robot& current_robot();

#endif // ROBOT_HPP_
