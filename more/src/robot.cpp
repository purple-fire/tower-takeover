#include "robot.hpp"

using namespace purpl::literals;

constexpr double RECLINER_RATIO = 12.0 / 84.0;
constexpr double ARMS_RATIO = 12.0 / 84.0;

constexpr purpl::QAngle RECLINER_POTENTIOMETER_FORWARD_MAX = 54_deg;
constexpr purpl::QAngle RECLINER_POTENTIOMETER_BACKWARD_MAX = 141_deg;
constexpr purpl::QAngle RECLINER_POTENTIOMETER_ARMS_VALUE = 122_deg;

static Robot* current_robot_instance = nullptr;

Robot::Robot() :
    drive_train(
            {2.0_in,
             10.05_in,
             {purpl::BrakeMode::brake, 1, 2},
             {purpl::BrakeMode::brake, -3, -4}},
            {'A', 1.3}),
    intake({{purpl::Gearset::green, 1.0}, purpl::BrakeMode::brake, 8, -9},
           200_rpm,
           75_rpm,
           1_in,
           {' '},
           {' '},
           2500),
    arms({{purpl::Gearset::red, ARMS_RATIO}, purpl::BrakeMode::hold, 6},
         {{purpl::Gearset::red, ARMS_RATIO}, purpl::BrakeMode::hold, -7},
         {'B'},
         360_deg / 1_s),
    recliner({{{purpl::Gearset::red, RECLINER_RATIO},
               purpl::BrakeMode::hold,
               -5},
              100_rpm * RECLINER_RATIO,
              {'C', true},
              RECLINER_POTENTIOMETER_FORWARD_MAX,
              RECLINER_POTENTIOMETER_BACKWARD_MAX,
              RECLINER_POTENTIOMETER_ARMS_VALUE}) {}

Robot::Drive& Robot::get_drive_train() {
    return drive_train;
}

Robot::Recliner& Robot::get_recliner() {
    return recliner;
}

Robot::IntakeSystem& Robot::get_intake() {
    return intake;
}

Robot::Arms& Robot::get_arms() {
    return arms;
}

void initialize_robot() {
    current_robot_instance = new Robot();
}

Robot& current_robot() {
    return *current_robot_instance;
}
