#ifndef PURPL_UTILS_HPP_
#define PURPL_UTILS_HPP_

#pragma once

namespace purpl {

/**
 * Class to make multiple operator() overloads easily for std::visit().
 */
template <class... Ts>
struct Visitor : Ts... {
    using Ts::operator()...;
};
template <class... Ts>
Visitor(Ts...)->Visitor<Ts...>;

} // namespace purpl

#endif // PURPL_UTILS_HPP_
