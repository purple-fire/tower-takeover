/**
 * Generic cutoff controller for limiting output.
 */

#pragma once

#ifndef PURPL_CUTOFF_FILTER_HPP_
#define PURPL_CUTOFF_FILTER_HPP_

#include <utility>

namespace purpl {

/**
 * Filter to limit a value to within certain bounds.
 *
 * @tparam Input Type of the input (and output) of this filter.
 */
template <typename Input>
struct CutoffFilter {
    /**
     * Construct a CutoffFilter from its bounds.
     *
     * @param min, max Minimum and maximum bounds for the output.
     */
    CutoffFilter(Input min, Input max) : min_bound(min), max_bound(max) {}

    /**
     * Apply the cutoff filter.
     *
     * @param value Input value.
     * @return value clamped to be between the bounds of the filter.
     */
    Input filter(Input value) const {
        return std::min(std::max(value, min_bound), max_bound);
    }

    /**
     * Minimum output bound.
     */
    Input min_bound;

    /**
     * Maximum output bound.
     */
    Input max_bound;
};

} // namespace purpl

#endif // PURPL_CUTOFF_FILTER_HPP_
