#include "robot.hpp"

using namespace purpl::literals;

constexpr double RECLINER_RATIO = 12.0 / 36.0 * 12.0 / 84.0;
constexpr double ARMS_RATIO = 12.0 / 84.0;
constexpr double INTAKE_UPPER_RATIO = 24.0 / 18.0;

constexpr purpl::QAngle RECLINER_POTENTIOMETER_FORWARD_MAX = 76_deg;
constexpr purpl::QAngle RECLINER_POTENTIOMETER_BACKWARD_MAX = 215_deg;
constexpr purpl::QAngle RECLINER_POTENTIOMETER_ARMS_VALUE = 180_deg;

static Robot* current_robot_instance = nullptr;

/*
 * Drive Left (11, 13, 14)
 * Drive Right (17, 18, 20)
 * Intake Left (2, 4)
 * Intake Right (9, 8)
 * Arm Left (1)
 * Arm Right (10)
 * Recliner Left (12)
 * Recliner Right (19)
 *
 */

Robot::Robot() :
    drive_train(
            {2.0_in,
             26.0_cm,
             {purpl::BrakeMode::brake, 11, 13, 14},
             {purpl::BrakeMode::brake, -17, -18, -20}},
            {'A'}),
    intake({purpl::Motor{1.0, purpl::BrakeMode::brake, 2},
            purpl::Motor{INTAKE_UPPER_RATIO, purpl::BrakeMode::brake, 4},
            purpl::Motor{1.0, purpl::BrakeMode::brake, -9},
            purpl::Motor{INTAKE_UPPER_RATIO, purpl::BrakeMode::brake, -8}},
           200_rpm,
           75_rpm,
           1.25_in,
           {'G'},
           {'H'},
           2500),
    arms({{purpl::Gearset::red, ARMS_RATIO}, purpl::BrakeMode::hold, 1},
         {{purpl::Gearset::red, ARMS_RATIO}, purpl::BrakeMode::hold, -10},
         {'C'},
         360_deg / 1_s),
    recliner({{{purpl::Gearset::red, RECLINER_RATIO},
               purpl::BrakeMode::hold,
               12,
               -19},
              100_rpm * RECLINER_RATIO,
              {'B'},
              RECLINER_POTENTIOMETER_FORWARD_MAX,
              RECLINER_POTENTIOMETER_BACKWARD_MAX,
              RECLINER_POTENTIOMETER_ARMS_VALUE}) {}

Robot::Drive& Robot::get_drive_train() {
    return drive_train;
}

Robot::Recliner& Robot::get_recliner() {
    return recliner;
}

Robot::IntakeSystem& Robot::get_intake() {
    return intake;
}

Robot::Arms& Robot::get_arms() {
    return arms;
}

void initialize_robot() {
    current_robot_instance = new Robot();
}

Robot& current_robot() {
    return *current_robot_instance;
}
