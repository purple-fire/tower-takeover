#include "purpl/gyro.hpp"

namespace purpl {

Gyro::Gyro(std::uint8_t port, double multiplier) :
    pros::ADIGyro(port, multiplier) {}

QAngle Gyro::get_angle() const {
    return get_value() / 10.0 * degree;
}

void Gyro::reset() {
    pros::ADIGyro::reset();
}

} // namespace purpl
