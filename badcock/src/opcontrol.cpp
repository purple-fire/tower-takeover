#include "main.h"
#include "menu.hpp"
#include "purpl/math.hpp"
#include "robot.hpp"

#include <algorithm>
#include <cmath>

constexpr double DROP_OFF_MULTIPLIER = 1.5;

enum class ArmsPreset { bottom, low_tower, top };

static purpl::QAngle preset_angle(ArmsPreset preset) {
    switch (preset) {
    case ArmsPreset::bottom:
        return ARMS_BOTTOM_MAX;
    case ArmsPreset::low_tower:
        return ARMS_LOW_TOWER;
    case ArmsPreset::top:
        return ARMS_TOP_MAX;
    default:
        return ARMS_BOTTOM_MAX;
    }
}

static ArmsPreset next_preset_up(ArmsPreset preset) {
    switch (preset) {
    case ArmsPreset::bottom:
        return ArmsPreset::low_tower;
    case ArmsPreset::low_tower:
        return ArmsPreset::top;
    case ArmsPreset::top:
        // Don't go higher than this
        return ArmsPreset::top;
    default:
        return preset;
    }
}

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
    pros::Controller master(pros::E_CONTROLLER_MASTER);

    controller_menu.start_task();

    ReclinerState recliner_state = ReclinerState::backward;

    ArmsPreset arms_preset = ArmsPreset::bottom;

    bool outtake_mode = false;
    bool intake_direct = true;

    bool global_override = false;

    auto& robot = current_robot();
    auto& drive_train = robot.get_drive_train();
    auto& recliner = robot.get_recliner();
    auto& intake = robot.get_intake();
    auto& arms = robot.get_arms();

    drive_train.start_task();
    recliner.start_task();
    intake.start_task();

    drive_train.user_control();

    // arms.set_target(preset_angle(arms_preset));

    while (true) {
        auto joy_y = std::pow(master.get_analog(ANALOG_LEFT_Y) / 127.0, 3);
        auto joy_x =
                std::pow(master.get_analog(ANALOG_RIGHT_X) / 127.0, 3) / 2.0;

        auto left_vel = purpl::clamp(joy_y + joy_x, -1.0, 1.0) *
                        drive_train.get_max_speed();
        auto right_vel = purpl::clamp(joy_y - joy_x, -1.0, 1.0) *
                         drive_train.get_max_speed();

        drive_train.move_velocity(left_vel, right_vel);

        bool recliner_up_press =
                master.get_digital(pros::E_CONTROLLER_DIGITAL_L1);
        bool recliner_down_press =
                master.get_digital(pros::E_CONTROLLER_DIGITAL_L2);

        bool intake_in_press =
                master.get_digital(pros::E_CONTROLLER_DIGITAL_R1);
        bool intake_out_press =
                master.get_digital(pros::E_CONTROLLER_DIGITAL_R2);

        bool intake_out_bottom_press =
                master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_B);

        bool input_inhibit_press =
                master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_Y);
        if (input_inhibit_press) {
            controller_menu.toggle_input_inhibit();
        }

        // Only look at the Up and Down buttons if controller input is
        // inhibitted

        if (controller_menu.get_input_inhibit()) {
            if (master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_LEFT)) {
                global_override = !global_override;

                recliner.set_override(global_override);
            }

            if (global_override) {
                bool arms_up_held =
                        master.get_digital(pros::E_CONTROLLER_DIGITAL_UP);
                bool arms_down_held =
                        master.get_digital(pros::E_CONTROLLER_DIGITAL_DOWN);

                if (arms_up_held) {
                    arms.move_velocity(arms.get_max_speed());
                } else if (arms_down_held) {
                    arms.move_velocity(-arms.get_max_speed());
                } else {
                    arms.move_velocity(0_rpm);
                }
            } else {
                bool arms_up_press = master.get_digital_new_press(
                        pros::E_CONTROLLER_DIGITAL_UP);
                bool arms_down_press = master.get_digital_new_press(
                        pros::E_CONTROLLER_DIGITAL_DOWN);
                if (arms_up_press) {
                    arms_preset = next_preset_up(arms_preset);
                    arms.set_target(preset_angle(arms_preset));
                    recliner_state = ReclinerState::free_arms;
                } else if (arms_down_press) {
                    // Just go to bottom
                    arms_preset = ArmsPreset::bottom;
                    arms.set_target(preset_angle(arms_preset));
                }
            }

            bool outtake_mode_press =
                    master.get_digital_new_press(pros::E_CONTROLLER_DIGITAL_X);

            if (recliner_up_press) {
                recliner_state = ReclinerState::forward;
            } else if (recliner_down_press) {
                recliner_state = ReclinerState::backward;
            } else if (recliner_state != ReclinerState::free_arms) {
                recliner_state = ReclinerState::off;
            }
            recliner.set_state(recliner_state);

            if (outtake_mode_press) {
                outtake_mode = !outtake_mode;
            }

            if (outtake_mode) {
                intake_direct = false;
                const auto wheel_vels = drive_train.get_velocities();
                const auto avg_vel =
                        (wheel_vels.first + wheel_vels.second) / 2.0;
                intake.set_linear_velocity(DROP_OFF_MULTIPLIER * avg_vel);
            } else if (intake_out_bottom_press) {
                intake_direct = false;
                outtake_mode = false;
                intake.outtake_to_bottom();
            } else if (intake_in_press) {
                intake_direct = true;
                outtake_mode = false;
                intake.intake();
            } else if (intake_out_press) {
                intake_direct = true;
                outtake_mode = false;
                intake.outtake();
            } else if (intake_direct) {
                // Onyl set speed to 0 if not running the outtake_to_bottom
                // routine
                intake.set_velocity(0_rpm);
            }
        }

        pros::delay(20);
    }
}
