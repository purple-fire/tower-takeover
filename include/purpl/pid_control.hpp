/**
 * Generic PID controller logic.
 */

#pragma once

#ifndef PURPL_PID_CONTROL_HPP_
#define PURPL_PID_CONTROL_HPP_

#include <utility>

namespace purpl {

/**
 * PID controller.
 *
 * This is generic in the Time, Input, and Output types so that it can be used
 * with the PURPL units library (see purpl/units.hpp).
 *
 * TODO Can/should Output ever be anything but Input / Time?
 */
template <
        typename Time,
        typename Input,
        typename Output =
                decltype(std::declval<Input>() / std::declval<Time>())>
class PIDController {
public:
    /**
     * Type of the integral term.
     */
    using IntegralType = decltype(std::declval<Input>() * std::declval<Time>());

    /**
     * Type of the derivative term.
     */
    using DerivativeType =
            decltype(std::declval<Input>() / std::declval<Time>());

    /**
     * PID configuration parameters.
     */
    struct Gains {
        using KpType = decltype(std::declval<Output>() / std::declval<Input>());
        using KiType =
                decltype(std::declval<Output>() / std::declval<IntegralType>());
        using KdType = decltype(
                std::declval<Output>() / std::declval<DerivativeType>());

        /**
         * P gain.
         */
        KpType kp;

        /**
         * I gain.
         */
        KiType ki;

        /**
         * D gain.
         */
        KdType kd;
    };

    /**
     * Construct a PID controller from the gain parameters.
     */
    PIDController(Gains gains) :
        gains(gains), target(), last_error(), integral() {}

    /**
     * Returns the current target input value.
     *
     * @return Current target value. This is returned by-value since PID
     * controllers are generally used with very small structures or primitive
     * values.
     */
    Input get_target() const {
        return target;
    }

    /**
     * Sets the target input.
     *
     * @param target New target value.
     */
    void set_target(Input target) {
        this->target = target;
    }

    /**
     * Returns the most recent error value.
     *
     * @return Most recent error value (i.e. target - input).
     */
    Input get_error() const {
        return last_error;
    }

    /**
     * Give the controller a new input value and return the new output.
     *
     * @param delta Time delta from the last call to this function.
     * @param current Current value of the input.
     * @return New value of the output.
     */
    Output step(Time delta, Input current) {
        const auto error = target - current;

        accumulate_integral(delta, error);
        const auto diff = (error - last_error) / delta;

        const auto p = gains.kp * error;
        const auto i = gains.ki * integral;
        const auto d = gains.kd * diff;

        last_error = error;

        return p + i + d;
    }

    /**
     * Resets the integral and error values (but not the target).
     */
    void reset() {
        last_error = Input();
        integral = IntegralType();
    }

    const Gains& get_gains() const {
        // TODO Return by value? Gains is fairly small.
        return gains;
    }

    void set_gains(const Gains& gains) {
        // TODO Pass gains by value?
        this->gains = gains;
    }

private:
    /**
     * Accumulate a value into the integral sum.
     *
     * @param delta Time delta from the last call to this function.
     * @param current Current value of the input.
     *
     * TODO Add overflow handling / maximum limiting
     */
    void accumulate_integral(Time delta, Input next) {
        integral += next * delta;
    }

    /**
     * PID gains of this controller.
     */
    Gains gains;

    /**
     * Current target input value.
     */
    Input target;

    /**
     * Error from the last input value.
     */
    Input last_error;

    /**
     * Accumulated integral of all past errors (with some conditions).
     */
    IntegralType integral;
};

} // namespace purpl

#endif // PURPL_PID_CONTROL_HPP_
