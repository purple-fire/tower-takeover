/**
 * Miscellaneous math functions (particularly for use with the units library).
 */

#pragma once

#ifndef PURPL_MATH_HPP_
#define PURPL_MATH_HPP_

#include <algorithm>

namespace purpl {

template <typename T>
constexpr T abs(const T& value) {
    return value >= T() ? value : -value;
}

template <typename T, typename S>
constexpr T copysign(const T& value, const S& sign) {
    const auto abs_value = abs(value);
    return sign >= S() ? abs_value : -abs_value;
}

template <typename T>
constexpr T clamp(const T& val, const T& low, const T& high) {
    return std::max(std::min(val, high), low);
}

} // namespace purpl

#endif // PURPL_MATH_HPP_
