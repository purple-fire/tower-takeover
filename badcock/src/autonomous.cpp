#include "common/program_selection.hpp"
#include "main.h"
#include "menu.hpp"
#include "purpl/math.hpp"
#include "robot.hpp"

using Drive = std::decay_t<decltype(current_robot().get_drive_train())>;
using TranslationConfig = Drive::TranslationControl::Config;
using RotationConfig = Drive::RotationControl::Config;

static TranslationConfig fast_move_config{
        {3.3_Hz, 0.75_Hz * 1.0_Hz, 0.1_num},
        1_cm,
        4_mps,
        8_mps2,
};

static TranslationConfig const_move_config{
        {100_Hz, 0_Hz * 1_Hz, 0_num},
        5_cm,
        4_mps,
        100_mps2,
};

static RotationConfig fast_rotate_config{
        {3.0_Hz, 0.07_Hz * 1.0_Hz, 0.1_num},
        1_deg,
        185_dps,
        450_dps / 1_s,
};

static TranslationConfig cube_move_config{
        {3.3_Hz, 0.1_Hz * 1.0_Hz, 0.05_num},
        1_cm,
        0.7_mps,
        2.6_mps2,
};

static TranslationConfig slow_move_config{
        {3.3_Hz, 0.1_Hz * 1.0_Hz, 0.05_num},
        1_cm,
        1.3_mps,
        2.6_mps2,
};

static TranslationConfig pick_up_move_config{
        {3.3_Hz, 0.75_Hz * 1.0_Hz, 0.05_num},
        1_cm,
        0.2_mps,
        6_mps2,
};

static RotationConfig slow_rotate_config{
        {3.0_Hz, 0.005_Hz * 1.0_Hz, 0.01_num},
        1_deg,
        100_dps,
        300_dps / 1_s,
};

class Autonomous {
public:
    Autonomous(Robot& robot) :
        robot(robot),
        drive_train(robot.get_drive_train()),
        recliner(robot.get_recliner()),
        intake(robot.get_intake()),
        arms(robot.get_arms()) {}
    ReclinerState recliner_state = ReclinerState::backward;

    Robot& robot;
    Robot::Drive& drive_train;
    Robot::Recliner& recliner;
    Robot::IntakeSystem& intake;
    Robot::Arms& arms;

    void run() {
        constexpr bool blue_side =
                PROGRAM == Program::blue || PROGRAM == Program::skills;
        constexpr bool red_side = !blue_side;

        drive_train.start_task();
        recliner.start_task();
        intake.start_task();

        unlock();

        //
        // Pick up the first line of cubes (L-shaped stack)
        //

        // Grab the first 2 in the L-shaped stack
        intake.intake();
        pick_up(20_in);
        wait_move_settled();
        pros::delay(500);
        intake.stop();

        // Move the arms up to grab the top cube
        arms.set_target(115_deg);
        // Outtake a bit so that a cube is in the intake. This widens the intake
        // and helps the robot not just push the cube when it moves forward
        intake.outtake_to_bottom();
        pros::delay(1000);
        // Drive forward to intake the top cube
        intake.set_velocity(150_rpm);
        pick_up(8_in);
        pros::delay(1200);
        // Move the arms down to get the bottom one
        arms.set_target(ARMS_BOTTOM_MAX);
        wait_move_settled();
        pros::delay(1000);
        intake.stop();

        // Move back to the wall
        const_move(-27_in);
        // Don't have to be precise since we are bumping against the wall next
        drive_train.wait_move_settled(5_cm);

        slam_wall();

        pros::delay(750);

        //
        // Pick up the second line of cubes
        //

        // Move forward a bit so it can turn
        move_cube(4_in);
        wait_move_settled();

        if constexpr (blue_side) {
            rotate_slow(-90_deg);
        } else {
            rotate_slow(90_deg);
        }
        wait_rotation_settled();

        // Move over to the next line of cubes
        move_cube(26_in);
        wait_move_settled();

        // Face the line
        if constexpr (blue_side) {
            rotate_slow(0_deg);
        } else {
            rotate_slow(0_deg);
        }
        wait_rotation_settled();

        // Intake the line
        intake.intake();
        pick_up(29_in);
        wait_move_settled();
        intake.outtake_to_bottom();

        // Turn to get the cube at the base of the tower
        if constexpr (blue_side) {
            rotate_slow(10_deg);
        } else {
            rotate_slow(-10_deg);
        }
        wait_rotation_settled();

        // Pick it up
        intake.intake();
        pick_up(9_in);
        wait_move_settled();
        pros::delay(600);
        intake.stop();

        // Back away from the tower so it can turn
        move_cube(-6_in);
        wait_move_settled();

        // Go back to the wall again
        if constexpr (blue_side) {
            rotate_slow(-5_deg);
        } else {
            rotate_slow(5_deg);
        }
        wait_rotation_settled();

        move_cube(-36_in);
        drive_train.wait_move_settled(5_cm);

        slam_wall();

        pros::delay(750);

        //
        // Drop off the stack
        //

        // Move the stack to the right position now that we're done intaking
        // cubes and bumping the wall
        intake.outtake_to_bottom();

        // Move away from the wall so it can turn
        move_cube(8_in);
        wait_move_settled();

        // Turn to the stacking zone
        if constexpr (blue_side) {
            rotate_slow(-120_deg);
        } else {
            rotate_slow(120_deg);
        }
        wait_rotation_settled();

        // Move close to the zone
        move_cube(3_in);
        wait_move_settled();

        // Drive up againse the railing
        drive_goal();

        // Drop off the stack
        destack();

        // Back away and put the recliner back down
        intake.outtake();
        move_cube(-34_in);
        // Stop the intake after we clear the stack
        drive_train.wait_move_settled(10_in, 0_s);
        intake.stop();

        wait_move_settled();

        // Score a tower in skills, just position the robot to go forward
        // in matches
        if constexpr (PROGRAM == Program::skills) {
            //
            // Score a cube in the low tower
            //

            rotate_slow(-290_deg);
            wait_rotation_settled();

            recliner.set_state(ReclinerState::free_arms);
            recliner.wait_settled(5_deg);

            intake.intake();
            pick_up(15_in);
            wait_move_settled();

            intake.stop();
            move_cube(-6_in);
            wait_move_settled();

            arms.set_target(ARMS_LOW_TOWER);
            pros::delay(700);

            rotate_slow(-80_deg);
            wait_rotation_settled();

            move_cube(27_in);
            wait_move_settled();

            intake.outtake();
            pros::delay(1000);
            intake.stop();

            move_fast(-6_in);
            wait_move_settled();
        } else {
            // Back up and put the recliner down
            if constexpr (blue_side) {
                rotate_slow(-290_deg);
            } else {
                rotate_slow(290_deg);
            }

            wait_rotation_settled();

            recliner.set_state(ReclinerState::backward);
        }
    }

private:
    void destack() {
        intake.stop();
        recliner.set_state(ReclinerState::forward);
        recliner.wait_settled(5_deg);
    }

    void unlock() {
        intake.outtake();
        pros::delay(600);
        intake.stop();
        // recliner.set_state(ReclinerState::free_arms);
        // intake.stop();
        // arms.set_target(ARMS_TOP_MAX);
        // pros::delay(1200);
        // arms.set_target(ARMS_BOTTOM_MAX);
        // recliner.set_state(ReclinerState::backward);
    }

    void drive_goal() {
        drive_train.user_control();
        drive_train.move_velocity(0.3 * drive_train.get_max_speed());
        pros::delay(600);
        drive_train.move_velocity(0_mps);
        pros::delay(600);
    }

    void slam_wall() {
        drive_train.user_control();
        drive_train.move_velocity(-0.25 * drive_train.get_max_speed());
        pros::delay(600);
        drive_train.move_velocity(0_mps);
        drive_train.tare_gyro();
    }

    void move_slow(purpl::QLength dist) {
        // drive_train.move(slow_move_config, dist);
        drive_train.move_internal(dist, 1.0_mps);
    }

    void move_cube(purpl::QLength dist) {
        // drive_train.move(cube_move_config, dist);
        drive_train.move_internal(dist, 0.5_mps);
    }

    void pick_up(purpl::QLength dist) {
        // drive_train.move(pick_up_move_config, dist);
        drive_train.move_internal(dist, 0.2_mps);
    }

    void move_fast(purpl::QLength dist) {
        // drive_train.move(fast_move_config, dist);
        drive_train.move_internal(dist, 2.0_mps);
    }

    void const_move(purpl::QLength dist) {
        // drive_train.move(const_move_config, dist);
        drive_train.move_internal(dist, 2.0_mps);
    }

    void rotate_slow(purpl::QAngle angle) {
        drive_train.rotate(slow_rotate_config, angle);
        // drive_train.rotate_internal(angle);
    }

    void rotate_fast(purpl::QAngle angle) {
        drive_train.rotate(fast_rotate_config, angle);
        // drive_train.rotate_internal(angle);
    }

    void wait_move_settled() {
        drive_train.wait_move_settled();
    }

    void wait_rotation_settled() {
        drive_train.wait_rotation_settled(2_deg);
    }

    purpl::QAngle bottom = ARMS_BOTTOM_MAX;
    purpl::QAngle low_tower = ARMS_LOW_TOWER;
    purpl::QAngle top = ARMS_TOP_MAX;
};

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {
    Autonomous bad_cock_auto(current_robot());
    bad_cock_auto.run();
}
