
#include "purpl/ui/controller_menu.hpp"

namespace purpl {

namespace ui {

//
// MenuEntry
//

MenuEntry::MenuEntry(std::string label, std::unique_ptr<Value> value) :
    label(label), value(std::move(value)) {}

const std::string& MenuEntry::render_label() const {
    return label;
}

bool MenuEntry::needs_update() const {
    return value->needs_update();
}

std::string MenuEntry::render_value() const {
    return value->render();
}

void MenuEntry::on_event(ControllerButton event, ControllerMenu& root_menu) {
    value->on_event(event, root_menu);
}

//
// Menu
//

Menu::Menu(std::vector<MenuEntry>&& entries) : entries(std::move(entries)) {}

Menu::Menu(Menu&& first, Menu&& second) {
    entries.reserve(first.entries.size() + second.entries.size());
    entries.insert(
            entries.end(),
            std::make_move_iterator(first.entries.begin()),
            std::make_move_iterator(first.entries.end()));
    entries.insert(
            entries.end(),
            std::make_move_iterator(second.entries.begin()),
            std::make_move_iterator(second.entries.end()));
}

bool Menu::needs_update() const {
    if (selection_dirty) {
        return true;
    }

    // Only look at rendered entries
    const std::uint8_t max_row =
            std::min({entries.size(), static_cast<size_t>(CONTROLLER_HEIGHT)});
    for (std::uint8_t i = 0; i < max_row; ++i) {
        if (entries[scroll_pos + i].needs_update()) {
            return true;
        }
    }

    return false;
}

void Menu::render(pros::Controller& controller) const {
    // TODO Only re-render entries which have changed (and hence don't clear
    // the screen every time). Also assume entry labels don't change?

    // There are 3 lines on the controller
    const std::uint8_t max_row =
            std::min({entries.size(), static_cast<size_t>(CONTROLLER_HEIGHT)});
    for (std::uint8_t i = 0; i < max_row; ++i) {
        const auto entry_idx = scroll_pos + i;
        const auto& entry = entries[entry_idx];
        if (selection_dirty || entry.needs_update()) {
            // Only re-render the entry if this specific entry needs it
            render_entry(controller, i, entry, entry_idx == current);
        }
    }

    // Write empty lines (i.e. clear) the rest
    for (std::uint8_t i = max_row; i < CONTROLLER_HEIGHT; ++i) {
        controller.set_text(i, 0, "");
        pros::delay(50);
    }

    // The screen's refresh rate is pretty low, so add some extra delay to
    // reduce flickering
    pros::delay(200);

    selection_dirty = false;
}

void Menu::on_event(ControllerButton event, ControllerMenu& root_menu) {
    // Shift the scroll position when the current selection moves off-screen.
    // TODO Instead have the current selection in the middle and show
    // arrows in the top and bottom margins to indicate when there are more
    // entries?
    switch (event) {
    case ControllerButton::down:
        if (current < entries.size() - 1) {
            ++current;
            if (current >= scroll_pos + CONTROLLER_HEIGHT) {
                ++scroll_pos;
            }
            selection_dirty = true;
        }
        break;
    case ControllerButton::up:
        if (current > 0) {
            --current;
            if (current < scroll_pos) {
                --scroll_pos;
            }
            selection_dirty = true;
        }
        break;
    default:
        entries[current].on_event(event, root_menu);
        break;
    }
}

void Menu::render_entry(
        pros::Controller& controller,
        std::uint8_t line,
        const MenuEntry& entry,
        bool is_current) const {
    // CONTROLLER_WIDTH = 15:
    // selection indicator: 1
    // label: 8 (left-justified)
    // separator: 1
    // value: 5 (right-justified)
    // TODO Give more space from label to value?

    auto value_str = entry.render_value();
    value_str.resize(5);

    controller.print(
            line,
            0,
            "%c%-8s %5s",
            is_current ? '>' : ' ',
            entry.render_label().c_str(),
            value_str.c_str());

    // From PROS docs: "updates faster than 10ms when on a wired connection or
    // 50ms over Vexnet will not be applied to the controller."
    pros::delay(50);
}

//
// ControllerMenu
//

ControllerMenu::ControllerMenu(
        pros::Controller controller, Menu&& initial_menu) :
    AsyncWrapper<ControllerMenu>(this),
    controller(controller),
    menu_stack({std::make_shared<Menu>(std::move(initial_menu))}) {}

void ControllerMenu::push_menu(std::shared_ptr<Menu> menu) {
    menu_stack.push(menu);
    menu_dirty = true;
}

void ControllerMenu::pop_menu() {
    if (menu_stack.size() > 1) {
        menu_stack.pop();
        menu_dirty = true;
    }
}

void ControllerMenu::poll_events() {
    if (input_inhibit) {
        return;
    }

    for (std::size_t i = 0; i < CONTROLLER_BUTTON_COUNT; ++i) {
        const auto pros_button = static_cast<pros::controller_digital_e_t>(
                i + MIN_CONTROLLER_BUTTON);
        const bool pressed = controller.get_digital(pros_button);
        if (pressed && !button_states[i]) {
            button_states[i] = true;
            on_event(static_cast<ControllerButton>(pros_button));
        } else if (button_states[i] && !pressed) {
            button_states[i] = false;
        }
    }
}

bool ControllerMenu::is_button_held(ControllerButton btn) {
    const auto pros_button = static_cast<pros::controller_digital_e_t>(btn);
    const bool pressed = controller.get_digital(pros_button);

    return pressed;
}

bool ControllerMenu::get_input_inhibit() const {
    return input_inhibit;
}

void ControllerMenu::toggle_input_inhibit() {
    input_inhibit = !input_inhibit;

    // Reset all the buttons for when input is allowed again
    if (input_inhibit) {
        for (std::size_t i = 0; i < CONTROLLER_BUTTON_COUNT; ++i) {
            button_states[i] = false;
        }
    }
}

bool ControllerMenu::needs_update() const {
    if (menu_dirty) {
        return true;
    }

    return menu_stack.top()->needs_update();
}

void ControllerMenu::render() {
    menu_stack.top()->render(controller);

    menu_dirty = false;
}

void ControllerMenu::loop() {
    controller.clear();
    pros::delay(50);

    render();

    while (true) {
        // TODO Run this in a separate thread so that it can take input while
        // rendering (which takes forever)
        poll_events();

        if (needs_update()) {
            render();
        } else {
            // render() has some sizeable delays during updating text on the
            // controller, so we only need to delay on this branch.
            pros::delay(50);
        }
    }
}

void ControllerMenu::on_event(ControllerButton event) {
    switch (event) {
    // Intercept B to go back
    case ControllerButton::b:
        pop_menu();
        break;
    default:
        menu_stack.top()->on_event(event, *this);
        break;
    }
}

//
// SubmenuValue
//

SubmenuValue::SubmenuValue(Menu&& menu) :
    menu(std::make_shared<Menu>(std::move(menu))) {}

void SubmenuValue::on_event(ControllerButton event, ControllerMenu& root_menu) {
    switch (event) {
    case ControllerButton::right:
    case ControllerButton::a:
        root_menu.push_menu(menu);
        break;
    default:
        break;
    }
}

std::unique_ptr<SubmenuValue> make_submenu_value(Menu&& menu) {
    return std::make_unique<SubmenuValue>(std::move(menu));
}

} // namespace ui

} // namespace purpl
