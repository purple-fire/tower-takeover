#pragma once

#ifndef DRIVE_TRAIN_HPP_
#define DRIVE_TRAIN_HPP_

#include "purpl/motor.hpp"
#include "purpl/units.hpp"

#include <utility>

using namespace purpl::literals;

template <class M>
class DriveTrain {
public:
    DriveTrain(
            purpl::QLength wheel_radius,
            purpl::QLength robot_width,
            M&& left_motors,
            M&& right_motors) :
        wheel_radius(wheel_radius),
        robot_width(robot_width),
        left_motors(std::move(left_motors)),
        right_motors(std::move(right_motors)) {}

    void
    move(purpl::QLength left,
         purpl::QLength right,
         purpl::QVelocity max_vel) const {
        left_motors.move_relative(dist_to_angle(left), vel_to_angular(max_vel));
        right_motors.move_relative(
                dist_to_angle(right), vel_to_angular(max_vel));
    }

    void turn(purpl::QAngle angle, purpl::QAngularVelocity max_vel) const {
        const auto linear_dist = turn_dist_to_linear(angle);
        const auto lienar_vel = turn_vel_to_linear(max_vel);
        move(linear_dist, -linear_dist, lienar_vel);
    }

    void move_velocity(purpl::QVelocity left, purpl::QVelocity right) const {
        left_motors.move_velocity(vel_to_angular(left));
        right_motors.move_velocity(vel_to_angular(right));
    }

    /**
     * Turn the robot clock-wise (looking down from the bottom) at the given
     * velocity.
     */
    void turn_velocity(purpl::QAngularVelocity velocity) const {
        const auto linear_vel = turn_vel_to_linear(velocity);
        move_velocity(linear_vel, -linear_vel);
    }

    /**
     * Gets the encoder values as distances moved.
     */
    std::pair<purpl::QLength, purpl::QLength> get_positions() const {
        auto left = left_motors.get_position();
        auto right = right_motors.get_position();

        return {angle_to_dist(left), angle_to_dist(right)};
    }

    /**
     * Gets the encoder values as distances moved.
     */
    std::pair<purpl::QLength, purpl::QLength> get_targets() const {
        auto left = left_motors.get_target_position();
        auto right = right_motors.get_target_position();

        return {angle_to_dist(left), angle_to_dist(right)};
    }

    /**
     * Gets the encoder values as distances moved.
     */
    std::pair<purpl::QVelocity, purpl::QVelocity> get_velocities() const {
        auto left = left_motors.get_velocity();
        auto right = right_motors.get_velocity();

        return {angular_to_vel(left), angular_to_vel(right)};
    }

    void reset() const {
        left_motors.tare_position();
        right_motors.tare_position();
    }

    purpl::QVelocity get_max_speed() const {
        // Assume they are the same (it wouldn't work well otherwise)
        return angular_to_vel(left_motors.get_max_speed());
    }

    purpl::QLength angle_to_dist(purpl::QAngle angle) const {
        return angle.convert(360.0_deg) * 2.0_pi * wheel_radius;
    }

    purpl::QAngle dist_to_angle(purpl::QLength dist) const {
        return dist / wheel_radius * purpl::radian;
    }

    purpl::QAngularVelocity vel_to_angular(purpl::QVelocity vel) const {
        return vel / wheel_radius * purpl::radian;
    }

    purpl::QVelocity angular_to_vel(purpl::QAngularVelocity vel) const {
        return vel * wheel_radius / purpl::radian;
    }

    purpl::QVelocity turn_vel_to_linear(purpl::QAngularVelocity vel) const {
        return vel / 1.0_rad * (robot_width / 2.0);
    }

    purpl::QLength turn_dist_to_linear(purpl::QAngle angle) const {
        return angle / 1.0_rad * (robot_width / 2.0);
    }

    purpl::QAngle dist_to_turn_angle(purpl::QLength dist) const {
        return dist / (robot_width / 2.0) * 1.0_rad;
    }

    M left_motors;
    M right_motors;

private:
    purpl::QLength wheel_radius;
    purpl::QLength robot_width;
};

#endif // DRIVE_TRAIN_HPP_
