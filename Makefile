ROBOT_PROJECTS := badcock more

define project_rule
.PHONY: $(1) $(1)-clean $(1)-format
$(1):
	@$(MAKE) -C $(1)
$(1)-clean:
	@$(MAKE) -C $(1) clean
$(1)-format:
	@$(MAKE) -C $(1) format
endef

all: $(ROBOT_PROJECTS)

.PHONY: clean
clean: $(foreach project, $(ROBOT_PROJECTS), $(project)-clean)

.PHONY: format
format: $(foreach project, $(ROBOT_PROJECTS), $(project)-format)

$(foreach project, $(ROBOT_PROJECTS), $(eval $(call project_rule, $(project))))
