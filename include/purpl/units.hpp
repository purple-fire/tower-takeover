/**
 * Compile-time dimenstional analysis library.
 * Source:
 * https://benjaminjurke.com/content/articles/2015/compile-time-numerical-unit-dimension-checking/
 *
 * TODO Split this up into multiple files / namespaces?
 * TODO Does there really need to be a `unsigned long long int` version of the
 * string literal operators?
 */

#pragma once

#ifndef PURPL_UNITS_HPP_
#define PURPL_UNITS_HPP_

#include <cmath>
#include <ratio>
#include <string>

namespace purpl {

template <
        typename MassDim,
        typename LengthDim,
        typename TimeDim,
        typename QAngleDim>
class RQuantity {
private:
    double value;

public:
    constexpr explicit RQuantity() : value(0.0) {}
    constexpr explicit RQuantity(double val) : value(val) {}

    // The intrinsic operations for a quantity with a unit is addition and
    // subtraction
    constexpr RQuantity const& operator+=(const RQuantity& rhs) {
        value += rhs.value;
        return *this;
    }
    constexpr RQuantity const& operator-=(const RQuantity& rhs) {
        value -= rhs.value;
        return *this;
    }

    constexpr RQuantity const operator-() const {
        return RQuantity<MassDim, LengthDim, TimeDim, QAngleDim>(-value);
    }

    // Returns the value of the quantity in multiples of the specified unit
    constexpr double convert(const RQuantity& rhs) const {
        return value / rhs.value;
    }

    // returns the raw value of the quantity (should not be used)
    constexpr double get_value() const {
        return value;
    }
};

// Standard arithmetic operators:
// ------------------------------

template <typename M, typename L, typename T, typename A>
constexpr RQuantity<M, L, T, A>
operator+(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return RQuantity<M, L, T, A>(lhs.get_value() + rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr RQuantity<M, L, T, A>
operator-(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return RQuantity<M, L, T, A>(lhs.get_value() - rhs.get_value());
}

template <
        typename M1,
        typename L1,
        typename T1,
        typename A1,
        typename M2,
        typename L2,
        typename T2,
        typename A2>
constexpr RQuantity<
        std::ratio_add<M1, M2>,
        std::ratio_add<L1, L2>,
        std::ratio_add<T1, T2>,
        std::ratio_add<A1, A2>>
operator*(
        const RQuantity<M1, L1, T1, A1>& lhs,
        const RQuantity<M2, L2, T2, A2>& rhs) {
    return RQuantity<
            std::ratio_add<M1, M2>,
            std::ratio_add<L1, L2>,
            std::ratio_add<T1, T2>,
            std::ratio_add<A1, A2>>(lhs.get_value() * rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr RQuantity<M, L, T, A>
operator*(const double& lhs, const RQuantity<M, L, T, A>& rhs) {
    return RQuantity<M, L, T, A>(lhs * rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr RQuantity<M, L, T, A>
operator*(const RQuantity<M, L, T, A>& lhs, const double& rhs) {
    return RQuantity<M, L, T, A>(lhs.get_value() * rhs);
}

template <
        typename M1,
        typename L1,
        typename T1,
        typename A1,
        typename M2,
        typename L2,
        typename T2,
        typename A2>
constexpr RQuantity<
        std::ratio_subtract<M1, M2>,
        std::ratio_subtract<L1, L2>,
        std::ratio_subtract<T1, T2>,
        std::ratio_subtract<A1, A2>>
operator/(
        const RQuantity<M1, L1, T1, A1>& lhs,
        const RQuantity<M2, L2, T2, A2>& rhs) {
    return RQuantity<
            std::ratio_subtract<M1, M2>,
            std::ratio_subtract<L1, L2>,
            std::ratio_subtract<T1, T2>,
            std::ratio_subtract<A1, A2>>(lhs.get_value() / rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr RQuantity<
        std::ratio_subtract<std::ratio<0>, M>,
        std::ratio_subtract<std::ratio<0>, L>,
        std::ratio_subtract<std::ratio<0>, T>,
        std::ratio_subtract<std::ratio<0>, A>>
operator/(double x, const RQuantity<M, L, T, A>& rhs) {
    return RQuantity<
            std::ratio_subtract<std::ratio<0>, M>,
            std::ratio_subtract<std::ratio<0>, L>,
            std::ratio_subtract<std::ratio<0>, T>,
            std::ratio_subtract<std::ratio<0>, A>>(x / rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr RQuantity<M, L, T, A>
operator/(const RQuantity<M, L, T, A>& rhs, double x) {
    return RQuantity<M, L, T, A>(rhs.get_value() / x);
}

// Comparison operators for quantities:
// ------------------------------------

template <typename M, typename L, typename T, typename A>
constexpr bool
operator==(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return (lhs.get_value() == rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr bool
operator!=(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return (lhs.get_value() != rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr bool
operator<=(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return (lhs.get_value() <= rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr bool
operator>=(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return (lhs.get_value() >= rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr bool
operator<(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return (lhs.get_value() < rhs.get_value());
}

template <typename M, typename L, typename T, typename A>
constexpr bool
operator>(const RQuantity<M, L, T, A>& lhs, const RQuantity<M, L, T, A>& rhs) {
    return (lhs.get_value() > rhs.get_value());
}

// Predefined (physical unit) quantity types:
// ------------------------------------------
#define QUANTITY_TYPE(_Mdim, _Ldim, _Tdim, _Adim, name)                        \
    using name = RQuantity<                                                    \
            std::ratio<_Mdim>,                                                 \
            std::ratio<_Ldim>,                                                 \
            std::ratio<_Tdim>,                                                 \
            std::ratio<_Adim>>;

// Replacement of "double" type
QUANTITY_TYPE(0, 0, 0, 0, Number);

// Physical quantity types
QUANTITY_TYPE(1, 0, 0, 0, QMass);
QUANTITY_TYPE(0, 1, 0, 0, QLength);
QUANTITY_TYPE(0, 2, 0, 0, QArea);
QUANTITY_TYPE(0, 3, 0, 0, QVolume);
QUANTITY_TYPE(0, 0, 1, 0, QTime);
QUANTITY_TYPE(0, 1, -1, 0, QVelocity);
QUANTITY_TYPE(0, 1, -2, 0, QAcceleration);
QUANTITY_TYPE(0, 1, -3, 0, QJerk);
QUANTITY_TYPE(0, 0, -1, 0, QFrequency);
QUANTITY_TYPE(1, 1, -2, 0, QForce);
QUANTITY_TYPE(1, -1, -2, 0, QPressure);

// QAngle type:
QUANTITY_TYPE(0, 0, 0, 1, QAngle);
QUANTITY_TYPE(0, 0, -1, 1, QAngularVelocity);
QUANTITY_TYPE(0, 0, -2, 1, QAngularAccel);

// Convenient conversion macro
#define CONVERT(src, unit) (src).convert(1.0_##unit)

// Predefined units:
// -----------------

constexpr Number number(1.0);

// Predefined mass units:
constexpr QMass kg(1.0); // SI base unit
constexpr QMass gramme = 0.001 * kg;
constexpr QMass tonne = 1000 * kg;
constexpr QMass ounce = 0.028349523125 * kg;
constexpr QMass pound = 16 * ounce;
constexpr QMass stone = 14 * pound;

// Predefined length-derived units
constexpr QLength metre(1.0); // SI base unit
constexpr QLength decimetre = metre / 10;
constexpr QLength centimetre = metre / 100;
constexpr QLength millimetre = metre / 1000;
constexpr QLength kilometre = 1000 * metre;
constexpr QLength inch = 2.54 * centimetre;
constexpr QLength foot = 12 * inch;
constexpr QLength yard = 3 * foot;
constexpr QLength mile = 5280 * foot;

constexpr QArea kilometre2 = kilometre * kilometre;
constexpr QArea metre2 = metre * metre;
constexpr QArea decimetre2 = decimetre * decimetre;
constexpr QArea centimetre2 = centimetre * centimetre;
constexpr QArea millimetre2 = millimetre * millimetre;
constexpr QArea inch2 = inch * inch;
constexpr QArea foot2 = foot * foot;
constexpr QArea mile2 = mile * mile;

constexpr QVolume kilometre3 = kilometre2 * kilometre;
constexpr QVolume metre3 = metre2 * metre;
constexpr QVolume decimetre3 = decimetre2 * decimetre;
constexpr QVolume litre = decimetre3;
constexpr QVolume centimetre3 = centimetre2 * centimetre;
constexpr QVolume millimetre3 = millimetre2 * millimetre;
constexpr QVolume inch3 = inch2 * inch;
constexpr QVolume foot3 = foot2 * foot;
constexpr QVolume mile3 = mile2 * mile;

// Predefined time-derived units:
constexpr QTime second(1.0); // SI base unit
constexpr QTime minute = 60 * second;
constexpr QTime hour = 60 * minute;
constexpr QTime day = 24 * hour;
constexpr QTime millisecond = second / 1000.0;
constexpr QTime microsecond = millisecond / 1000.0;

constexpr QFrequency Hz(1.0);

// Predefined mixed units:
constexpr QAcceleration G = 9.80665 * metre / (second * second);

constexpr QForce newton(1.0);
constexpr QForce poundforce = pound * G;
constexpr QForce kilopond = kg * G;

constexpr QPressure Pascal(1.0);
constexpr QPressure bar = 100000 * Pascal;
constexpr QPressure psi = pound * G / inch2;

// Predefined angle units:
constexpr QAngle radian(1.0);
constexpr QAngle degree = static_cast<double>(M_PI / 180.0) * radian;
constexpr QAngularVelocity rps = radian / second; // Radians per second
constexpr QAngularVelocity dps = degree / second; // Degrees per second
constexpr QAngularVelocity rpm =
        static_cast<double>(2.0 * M_PI) * radian / minute;

// Typesafe trignometric operations:
// ---------------------------------

inline double sin(const QAngle& theta) {
    return std::sin(theta.get_value());
}

inline double cos(const QAngle& theta) {
    return std::cos(theta.get_value());
}

inline double tan(const QAngle& theta) {
    return std::tan(theta.get_value());
}

// Physical unit literals:
// -----------------------
namespace literals {

// literals for unitless quantities
constexpr Number operator"" _num(long double x) {
    return Number(x);
}

constexpr Number operator"" _num(unsigned long long int x) {
    return Number(static_cast<double>(x));
}

// literals for length units
constexpr QLength operator"" _mm(long double x) {
    return static_cast<double>(x) * millimetre;
}
constexpr QLength operator"" _cm(long double x) {
    return static_cast<double>(x) * centimetre;
}
constexpr QLength operator"" _m(long double x) {
    return static_cast<double>(x) * metre;
}
constexpr QLength operator"" _km(long double x) {
    return static_cast<double>(x) * kilometre;
}
constexpr QLength operator"" _mi(long double x) {
    return static_cast<double>(x) * mile;
}
constexpr QLength operator"" _yd(long double x) {
    return static_cast<double>(x) * yard;
}
constexpr QLength operator"" _ft(long double x) {
    return static_cast<double>(x) * foot;
}
constexpr QLength operator"" _in(long double x) {
    return static_cast<double>(x) * inch;
}
constexpr QLength operator"" _mm(unsigned long long int x) {
    return static_cast<double>(x) * millimetre;
}
constexpr QLength operator"" _cm(unsigned long long int x) {
    return static_cast<double>(x) * centimetre;
}
constexpr QLength operator"" _m(unsigned long long int x) {
    return static_cast<double>(x) * metre;
}
constexpr QLength operator"" _km(unsigned long long int x) {
    return static_cast<double>(x) * kilometre;
}
constexpr QLength operator"" _mi(unsigned long long int x) {
    return static_cast<double>(x) * mile;
}
constexpr QLength operator"" _yd(unsigned long long int x) {
    return static_cast<double>(x) * yard;
}
constexpr QLength operator"" _ft(unsigned long long int x) {
    return static_cast<double>(x) * foot;
}
constexpr QLength operator"" _in(unsigned long long int x) {
    return static_cast<double>(x) * inch;
}

// literals for speed units
constexpr QVelocity operator"" _mps(long double x) {
    return QVelocity(x);
};
constexpr QVelocity operator"" _miph(long double x) {
    return static_cast<double>(x) * mile / hour;
};
constexpr QVelocity operator"" _kmph(long double x) {
    return static_cast<double>(x) * kilometre / hour;
};
constexpr QVelocity operator"" _mps(unsigned long long int x) {
    return QVelocity(static_cast<long double>(x));
};
constexpr QVelocity operator"" _miph(unsigned long long int x) {
    return static_cast<double>(x) * mile / hour;
};
constexpr QVelocity operator"" _kmph(unsigned long long int x) {
    return static_cast<double>(x) * kilometre / hour;
};

// literal for frequency unit
constexpr QFrequency operator"" _Hz(long double x) {
    return QFrequency(x);
};
constexpr QFrequency operator"" _Hz(unsigned long long int x) {
    return QFrequency(static_cast<long double>(x));
};

// literals for time units
constexpr QTime operator"" _s(long double x) {
    return QTime(x);
};
constexpr QTime operator"" _min(long double x) {
    return static_cast<double>(x) * minute;
};
constexpr QTime operator"" _h(long double x) {
    return static_cast<double>(x) * hour;
};
constexpr QTime operator"" _day(long double x) {
    return static_cast<double>(x) * day;
};
constexpr QTime operator"" _ms(long double x) {
    return static_cast<double>(x) * millisecond;
};
constexpr QTime operator"" _us(long double x) {
    return static_cast<double>(x) * microsecond;
};
constexpr QTime operator"" _s(unsigned long long int x) {
    return QTime(static_cast<double>(x));
};
constexpr QTime operator"" _min(unsigned long long int x) {
    return static_cast<double>(x) * minute;
};
constexpr QTime operator"" _h(unsigned long long int x) {
    return static_cast<double>(x) * hour;
};
constexpr QTime operator"" _day(unsigned long long int x) {
    return static_cast<double>(x) * day;
};
constexpr QTime operator"" _ms(unsigned long long int x) {
    return static_cast<double>(x) * millisecond;
};
constexpr QTime operator"" _us(unsigned long long int x) {
    return static_cast<double>(x) * microsecond;
};

// literals for mass units
constexpr QMass operator"" _kg(long double x) {
    return QMass(x);
};
constexpr QMass operator"" _g(long double x) {
    return static_cast<double>(x) * gramme;
};
constexpr QMass operator"" _t(long double x) {
    return static_cast<double>(x) * tonne;
};
constexpr QMass operator"" _oz(long double x) {
    return static_cast<double>(x) * ounce;
};
constexpr QMass operator"" _lb(long double x) {
    return static_cast<double>(x) * pound;
};
constexpr QMass operator"" _st(long double x) {
    return static_cast<double>(x) * stone;
};
constexpr QMass operator"" _kg(unsigned long long int x) {
    return QMass(static_cast<double>(x));
};
constexpr QMass operator"" _g(unsigned long long int x) {
    return static_cast<double>(x) * gramme;
};
constexpr QMass operator"" _t(unsigned long long int x) {
    return static_cast<double>(x) * tonne;
};
constexpr QMass operator"" _oz(unsigned long long int x) {
    return static_cast<double>(x) * ounce;
};
constexpr QMass operator"" _lb(unsigned long long int x) {
    return static_cast<double>(x) * pound;
};
constexpr QMass operator"" _st(unsigned long long int x) {
    return static_cast<double>(x) * stone;
};

// literals for acceleration units
constexpr QAcceleration operator"" _mps2(long double x) {
    return QAcceleration(x);
};
constexpr QAcceleration operator"" _mps2(unsigned long long int x) {
    return QAcceleration(static_cast<double>(x));
};
constexpr QAcceleration operator"" _G(long double x) {
    return static_cast<double>(x) * G;
};
constexpr QAcceleration operator"" _G(unsigned long long int x) {
    return static_cast<double>(x) * G;
}

// literals for force units
// TODO Figure out why the compiler doesn't like these 2 with a space before
// _N
constexpr QForce operator"" _n(long double x) {
    return QForce(x);
};
constexpr QForce operator"" _n(unsigned long long int x) {
    return QForce(static_cast<double>(x));
};
constexpr QForce operator"" _lbf(long double x) {
    return static_cast<double>(x) * poundforce;
};
constexpr QForce operator"" _lbf(unsigned long long int x) {
    return static_cast<double>(x) * poundforce;
};
constexpr QForce operator"" _kp(long double x) {
    return static_cast<double>(x) * kilopond;
};
constexpr QForce operator"" _kp(unsigned long long int x) {
    return static_cast<double>(x) * kilopond;
};

// literals for pressure units
constexpr QPressure operator"" _Pa(long double x) {
    return QPressure(x);
};
constexpr QPressure operator"" _Pa(unsigned long long int x) {
    return QPressure(static_cast<double>(x));
};
constexpr QPressure operator"" _bar(long double x) {
    return static_cast<double>(x) * bar;
};
constexpr QPressure operator"" _bar(unsigned long long int x) {
    return static_cast<double>(x) * bar;
};
constexpr QPressure operator"" _psi(long double x) {
    return static_cast<double>(x) * psi;
};
constexpr QPressure operator"" _psi(unsigned long long int x) {
    return static_cast<double>(x) * psi;
};

// Angular unit literals:
// ----------------------
constexpr long double operator"" _pi(long double x) {
    return static_cast<double>(x) * M_PI;
}
constexpr long double operator"" _pi(unsigned long long int x) {
    return static_cast<double>(x) * M_PI;
}

// literals for angle units
constexpr QAngle operator"" _rad(long double x) {
    return QAngle(x);
};
constexpr QAngle operator"" _rad(unsigned long long int x) {
    return QAngle(static_cast<double>(x));
};
constexpr QAngle operator"" _deg(long double x) {
    return static_cast<double>(x) * degree;
};
constexpr QAngle operator"" _deg(unsigned long long int x) {
    return static_cast<double>(x) * degree;
};
constexpr QAngularVelocity operator"" _rpm(long double x) {
    return static_cast<double>(x) * rpm;
};
constexpr QAngularVelocity operator"" _rpm(unsigned long long int x) {
    return static_cast<double>(x) * rpm;
};
constexpr QAngularVelocity operator"" _rps(long double x) {
    return static_cast<double>(x) * rps;
};
constexpr QAngularVelocity operator"" _rps(unsigned long long int x) {
    return static_cast<double>(x) * rps;
};
constexpr QAngularVelocity operator"" _dps(long double x) {
    return static_cast<double>(x) * dps;
};
constexpr QAngularVelocity operator"" _dps(unsigned long long int x) {
    return static_cast<double>(x) * dps;
};

} // namespace literals

template <typename M, typename L, typename T, typename A>
std::string
to_string_convert(RQuantity<M, L, T, A> val, RQuantity<M, L, T, A> units) {
    return std::to_string(val.convert(units));
}

} // namespace purpl

// Kind of a hack to make these usable in DynamicValue objects
// (units are discarded, however)
// ------------------------------

namespace std {

template <typename M, typename L, typename T, typename A>
string to_string(purpl::RQuantity<M, L, T, A> quantity) {
    return to_string(quantity.get_value());
}

} // namespace std

#endif // PURPL_UNITS_HPP_
