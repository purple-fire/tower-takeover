
#include "purpl/object_locator.hpp"

namespace purpl {

using namespace literals;

// Formula for these is:
//  fov_pixels / 2 / tan(fov_angle / 2)
// (they can't be constexpr because of tan(), which modifies errno)

static const double FOV_X_SCALE = VISION_FOV_WIDTH / 2 / tan(30_deg / 2);

static const double FOV_Y_SCALE = VISION_FOV_HEIGHT / 2 / tan(20_deg / 2);

static constexpr double FOCAL_LENGTH = 290.0;

ObjectLocator::ObjectLocator(QLength width, QLength height) :
    width(width), height(height) {}

QLength ObjectLocator::get_distance(const pros::vision_object_s_t& object) {
    // This seems to be more accurate just using width rather than averaging
    // width and height.
    // return width * FOCAL_LENGTH /
    //        std::sqrt((double)(object.width * object.height));
    return FOCAL_LENGTH * width / object.width;
}

QAngle ObjectLocator::get_yaw(const VisionObject& object) {
    return std::atan2(object.x_middle_coord, FOV_X_SCALE) * radian;
}

QAngle ObjectLocator::get_pitch(const VisionObject& object) {
    return std::atan2(object.y_middle_coord, FOV_Y_SCALE) * radian;
}

} // namespace purpl
