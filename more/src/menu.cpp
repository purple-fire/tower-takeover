
#include "menu.hpp"

#include "purpl/utils.hpp"
#include "robot.hpp"
#include "subsystems/speed_control_menu.hpp"

using namespace purpl::ui;
using namespace purpl::literals;

using Drive = std::decay_t<decltype(current_robot().get_drive_train())>;
using TranslationConfig = Drive::TranslationControl::Config;
using RotationConfig = Drive::RotationControl::Config;

// State for the menus

static TranslationConfig move_config{
        {4.5_Hz, 0.0_Hz * 1.0_Hz, 0.5_num},
        1_cm,
        1.5_mps,
        10_mps2,
};
static RotationConfig rotate_config{
        {3.0_Hz, 0.0_Hz * 1.0_Hz, 0.0_num},
        1_deg,
        45_dps,
        90_dps / 1_s,
};

/**
 * Renderer for the speed controller tuning menus.
 *
 * - cm for lengths
 * - mps and mps2 for other linear measurements
 * - angles in degrees
 */
static purpl::Visitor units_renderer{
        [](purpl::QLength length) {
            return purpl::to_string_convert(length, 1_cm);
        },
        [](purpl::QVelocity vel) {
            return purpl::to_string_convert(vel, 1_mps);
        },
        [](purpl::QAcceleration accel) {
            return purpl::to_string_convert(accel, 1_mps2);
        },

        [](purpl::QAngle angle) {
            return purpl::to_string_convert(angle, 1_deg);
        },
        [](purpl::QAngularVelocity vel) {
            return purpl::to_string_convert(vel, 1_dps);
        },
        [](purpl::QAngularAccel accel) {
            return purpl::to_string_convert(accel, 1_dps / 1_s);
        },
        [](auto val) { return std::to_string(val); },
};

static ControllerMenu generate_menu() {
    // The lambdas have to call current_robot() since the robot is not
    // initialized when this menu is generated.

    auto move_tuner = make_speed_control_menu<purpl::QTime, purpl::QLength>(
            move_config,
            {
                    {0.1_Hz, 0.001_Hz * 1_Hz, 0.01_num},
                    0.1_cm,
                    0.1_mps,
                    0.1_mps2,
            },
            []() {
                constexpr auto dist = 2_ft;
                current_robot().get_drive_train().move(move_config, dist);
            },
            []() { current_robot().get_drive_train().user_control(); },
            units_renderer);

    auto rotate_tuner = make_speed_control_menu<purpl::QTime, purpl::QAngle>(
            rotate_config,
            {
                    {0.1_Hz, 0.001_Hz * 1_Hz, 0.01_num},
                    0.1_deg,
                    5_dps,
                    5_dps / 1_s,
            },
            []() {
                constexpr auto angle = 90_deg;
                current_robot().get_drive_train().rotate(rotate_config, angle);
            },
            []() { current_robot().get_drive_train().user_control(); },
            units_renderer);

    MenuEntry move_submenu(
            "Move PID", make_submenu_value(std::move(move_tuner)));
    MenuEntry rotate_submenu(
            "Rot PID", make_submenu_value(std::move(rotate_tuner)));

    MenuEntry gyro_entry("Gyro", make_continuous_value([]() {
                             return current_robot()
                                     .get_drive_train()
                                     .gyro.get_angle()
                                     .convert(1_deg);
                         }));

    MenuEntry recliner_entry("Recliner", make_continuous_value([]() {
                                 return current_robot()
                                         .get_recliner()
                                         .get_potentiometer_angle()
                                         .convert(1_deg);
                             }));

    MenuEntry arms_entry(
            "Arms", make_continuous_value([]() {
                return current_robot().get_arms().get_pot_angle().convert(
                        1_deg);
            }));

    return {pros::Controller(pros::E_CONTROLLER_MASTER),
            {
                    std::move(move_submenu),
                    std::move(rotate_submenu),
                    std::move(gyro_entry),
                    std::move(recliner_entry),
                    std::move(arms_entry),
            }};
}

ControllerMenu controller_menu = generate_menu();
