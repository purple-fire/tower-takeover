/**
 * An extension around pros::Vision which allows taking "snapshots" of vision
 * objects.
 */

#pragma once

#ifndef PURPL_VISION_HPP_
#define PURPL_VISION_HPP_

#include "pros/vision.hpp"

#include <vector>

namespace purpl {

using VisionObject = pros::vision_object_s_t;

/**
 * A set of VisionObjects, ordered by size, from a point in time.
 *
 * TODO Make this a stream of objects rather than a vector so that
 * 1) All objects don't have to be loaded at once
 * 2) Filter operators don't have to copy objects (as much)
 */
class Snapshot {
public:
    /**
     * Make a snapshot from a list of objects.
     *
     * @param objects VisionObjects sorted by size.
     */
    Snapshot(std::vector<VisionObject> objects) : objects(objects){};

    /**
     * Number of objects in the snapshot.
     */
    size_t size() const {
        return objects.size();
    }

    /**
     * Whether the snapshot is empty.
     */
    bool empty() const {
        return objects.empty();
    }

    using VectorType = std::vector<VisionObject>;
    using Iterator = VectorType::const_iterator;

    /**
     * Iterator to the largest (first) object in the snapshot.
     */
    Iterator begin() const {
        return objects.begin();
    }

    /**
     * Iterator to the smallest (last) object in the snapshow.
     */
    Iterator end() const {
        return objects.end();
    }

    /**
     * Filter the objects in the list by a predicate.
     *
     * @param f Function object taking a const VisionObject& and returning a
     * bool.
     */
    template <typename F>
    void filter(F predicate);

private:
    std::vector<VisionObject> objects;
};

class Vision : public pros::Vision {
public:
    /**
     * Constructor which forwards to pros::Vision()
     */
    Vision(uint8_t port,
           pros::vision_zero_e_t zero_point = pros::E_VISION_ZERO_TOPLEFT);

    /**
     * Takes a snapshot of all recognised objects.
     */
    Snapshot snapshot_all();

    /**
     * Takes a snapshot of all objects of a particular color signature.
     */
    Snapshot snapshot_sig(uint32_t sig_id);

    /**
     * Takes a snapshot of all objects of a particular color code set.
     */
    Snapshot snapshot_code(uint32_t code_id);
};

} // namespace purpl

#endif // PURPL_VISION_HPP_
