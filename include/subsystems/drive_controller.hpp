#pragma once

#ifndef DRIVE_CONTROLLER_HPP_
#define DRIVE_CONTROLLER_HPP_

#include "drive_train.hpp"
#include "purpl/accel_limit_control.hpp"
#include "purpl/async_wrapper.hpp"
#include "purpl/gyro.hpp"
#include "purpl/math.hpp"
#include "purpl/pid_control.hpp"
#include "purpl/utils.hpp"
#include "speed_control.hpp"

#include <optional>
#include <variant>

template <typename M>
class DriveController :
    private DriveTrain<M>,
    public purpl::AsyncWrapper<DriveController<M>> {
public:
    using TranslationControl = SpeedController<purpl::QTime, purpl::QLength>;

    using RotationControl = SpeedController<purpl::QTime, purpl::QAngle>;

    /**
     * Constructs a drive train controller.
     *
     * Note: this does NOT start the task. Use start_task() to start the task.
     */
    DriveController(DriveTrain<M>&& drive_train, purpl::Gyro&& gyro) :
        purpl::AsyncWrapper<DriveController<M>>(this, "DriveController"),
        DriveTrain<M>(std::move(drive_train)),
        gyro(std::move(gyro)) {}

    DriveController(const DriveController&) = delete;
    DriveController& operator=(const DriveController&) = delete;

    void move_velocity(purpl::QVelocity left, purpl::QVelocity right) const {
        if (std::holds_alternative<UserControlState>(state)) {
            DriveTrain<M>::move_velocity(left, right);
        }
    }

    void turn_velocity(purpl::QAngularVelocity velocity) const {
        if (std::holds_alternative<UserControlState>(state)) {
            DriveTrain<M>::turn_velocity(velocity);
        }
    }

    void move_velocity(purpl::QVelocity speed) const {
        move_velocity(speed, speed);
    }

    using DriveTrain<M>::get_positions;
    using DriveTrain<M>::get_velocities;
    using DriveTrain<M>::get_max_speed;

    /**
     * Enable user control (move_velocity() and turn_velocity()).
     *
     * User control is disabled by calling one of the non-user-control functions
     * (move() and rotate()).
     */
    void user_control() {
        state = UserControlState{};
    }

    void rotate(RotationControl::Config config, purpl::QAngle angle) {
        RotationControl controller(config);
        controller.set_target(angle);
        state = RotateState{controller};
    }

    void
    move(TranslationControl::Config config,
         purpl::QLength left,
         purpl::QLength right) {
        TranslationControl left_controller(config);
        TranslationControl right_controller(config);
        const auto cur_pos = get_positions();
        left_controller.set_target(cur_pos.first + left);
        right_controller.set_target(cur_pos.second + right);

        state = MoveState{left_controller, right_controller};
        // TODO Also run the rotation controller to try to keep things even?
    }

    void move(TranslationControl::Config config, purpl::QLength dist) {
        move(config, dist, dist);
    }

    void rotate_internal(purpl::QAngle angle, purpl::QAngularVelocity max_vel) {
        DriveTrain<M>::turn(angle, max_vel);
        state = InternalState{};
    }

    void rotate_internal(purpl::QAngle angle) {
        rotate_internal(angle, 200_dps);
    }

    void move_internal(
            purpl::QLength left,
            purpl::QLength right,
            purpl::QVelocity max_vel) {
        DriveTrain<M>::move(left, right, max_vel);
        state = InternalState{};
    }

    void move_internal(purpl::QLength dist, purpl::QVelocity max_vel) {
        move_internal(dist, dist, max_vel);
    }

    void move_internal(purpl::QLength dist) {
        move_internal(dist, get_max_speed());
    }

    void stop() {
        state = StopState();
    }

    void reset() {
        DriveTrain<M>::reset();
        stop();
    }

    void tare_gyro() {
        gyro.reset();
    }

    std::optional<std::pair<purpl::QLength, purpl::QLength>>
    get_move_errors() const {
        if (auto move_state = std::get_if<MoveState>(&state)) {
            return {{move_state->left_controller.get_error(),
                     move_state->right_controller.get_error()}};
        } else {
            return {};
        }
    }

    std::optional<purpl::QAngle> get_rotate_errors() const {
        if (auto rotate_state = std::get_if<RotateState>(&state)) {
            return rotate_state->rotation_controller.get_error();
        } else {
            return {};
        }
    }

    void wait_move_settled(
            purpl::QLength tol = DEFAULT_MOVE_TOLERANCE,
            purpl::QTime duration = DEFAULT_SETTLE_DURATION) {
        const std::uint32_t dur_millis = duration.convert(1_ms);
        purpl::make_wait_settled_func(LOOP_DELAY, dur_millis, tol, [&]() {
            purpl::Visitor handler{
                    [&](MoveState& state) {
                        const auto left_err = state.left_controller.get_error();
                        const auto right_err =
                                state.right_controller.get_error();

                        // Both have to be in tolerance
                        return std::max(
                                purpl::abs(left_err), purpl::abs(right_err));
                    },
                    [&](InternalState) {
                        const auto targets = DriveTrain<M>::get_targets();
                        const auto positions = DriveTrain<M>::get_positions();
                        return std::max(
                                purpl::abs(positions.first - targets.first),
                                purpl::abs(positions.second - targets.second));
                    },
                    [&](auto) { return purpl::QLength(); },
            };
            return std::visit(handler, state);
        });
    }

    void wait_rotation_settled(
            purpl::QAngle tol = DEFAULT_ROTATE_TOLERANCE,
            purpl::QTime duration = DEFAULT_SETTLE_DURATION) {
        const std::uint32_t dur_millis = duration.convert(1_ms);
        purpl::make_wait_settled_func(LOOP_DELAY, dur_millis, tol, [&]() {
            purpl::Visitor handler{
                    [&](RotateState& state) {
                        return state.rotation_controller.get_error();
                    },
                    [&](InternalState) {
                        const auto targets = DriveTrain<M>::get_targets();
                        const auto positions = DriveTrain<M>::get_positions();
                        return DriveTrain<M>::dist_to_turn_angle(std::max(
                                purpl::abs(positions.first - targets.first),
                                purpl::abs(positions.second - targets.second)));
                    },
                    [&](auto) { return purpl::QAngle(); },
            };
            return std::visit(handler, state);
        });
    }

    /**
     * Control loop
     */
    void loop() {
        purpl::make_async_loop(LOOP_DELAY, [&](std::uint32_t delay_millis) {
            const auto delay = delay_millis * purpl::millisecond;
            purpl::Visitor handler{
                    [&](MoveState& state) {
                        const auto encoders = get_positions();
                        const auto left_vel = state.left_controller.step(
                                delay, encoders.first);
                        const auto right_vel = state.right_controller.step(
                                delay, encoders.second);

                        // TODO Run rotation PID to keep the angle fixed?
                        // const auto angle = gyro.get_angle();
                        // const auto angular_vel =
                        //         rotation_controller.step(prev_delay, angle);
                        // const auto rotation_vel =
                        // angular_to_vel(angular_vel);
                        // DriveTrain<M>::move_velocity(
                        //         left_vel + rotation_vel, right_vel -
                        //         rotation_vel);

                        DriveTrain<M>::move_velocity(left_vel, right_vel);
                    },
                    [&](RotateState& state) {
                        const auto angle = gyro.get_angle();
                        const auto angular_vel =
                                state.rotation_controller.step(delay, angle);
                        DriveTrain<M>::turn_velocity(angular_vel);
                    },
                    [&](StopState) {
                        DriveTrain<M>::move_velocity(
                                purpl::QVelocity(), purpl::QVelocity());
                    },
                    [&](auto) {}};
            std::visit(handler, state);
        });
    }

    // TODO Make these private again once PID tuning is done
    // (exposing setters is a lot of extra boilerplate)

    purpl::Gyro gyro;

private:
    static constexpr std::uint32_t LOOP_DELAY = 20;
    static constexpr purpl::QLength DEFAULT_MOVE_TOLERANCE = 0.5_in;
    static constexpr purpl::QAngle DEFAULT_ROTATE_TOLERANCE = 1_deg;
    static constexpr purpl::QTime DEFAULT_SETTLE_DURATION = 0.2_s;

    using DriveTrain<M>::left_motors;
    using DriveTrain<M>::right_motors;

    struct MoveState {
        TranslationControl left_controller;
        TranslationControl right_controller;
    };

    struct RotateState {
        RotationControl rotation_controller;
    };

    struct StopState {};
    struct UserControlState {};
    struct InternalState {};

    using State = std::variant<
            MoveState,
            RotateState,
            StopState,
            UserControlState,
            InternalState>;

    State state = StopState{};
};

#endif // DRIVE_CONTROLLER_HPP_
