#ifndef SPEED_CONTROL_MENU_HPP_
#define SPEED_CONTROL_MENU_HPP_

#include "purpl/ui/controller_menu.hpp"
#include "speed_control.hpp"

/**
 * @param config Reference to a config value defined elsewhere. This MUST have a
 * lifetime longer than this menu (i.e. should usually be static).
 * TODO Take a std::shared_ptr instead?
 */
template <
        typename Time,
        typename Input,
        typename LoopRunner,
        typename LoopStopper,
        typename Renderer>
purpl::ui::Menu make_speed_control_menu(
        typename SpeedController<Time, Input>::Config& config,
        typename SpeedController<Time, Input>::Config increments,
        LoopRunner run_loop,
        LoopStopper stop_loop,
        Renderer render) {
    using namespace purpl::ui;

    using Gains = typename purpl::PIDController<Time, Input>::Gains;
    using KpType = typename Gains::KpType;
    using KiType = typename Gains::KiType;
    using KdType = typename Gains::KdType;

    MenuEntry tol_entry(
            "Tol",
            make_controlled_value(
                    increments.tol,
                    [&]() { return config.tol; },
                    [&](auto tol) { config.tol = tol; },
                    [=](auto val) { return render(val); }));

    MenuEntry vel_entry(
            "Vel",
            make_controlled_value(
                    increments.max_vel,
                    [&]() { return config.max_vel; },
                    [&](auto vel) { config.max_vel = vel; },
                    [=](auto val) { return render(val); }));

    MenuEntry accel_entry(
            "Accel",
            make_controlled_value(
                    increments.max_accel,
                    [&]() { return config.max_accel; },
                    [&](auto accel) { config.max_accel = accel; },
                    [=](auto val) { return render(val); }));

    MenuEntry kp_entry(
            "Kp",
            make_controlled_value(
                    increments.gains.kp,
                    [&]() { return config.gains.kp; },
                    [&](KpType kp) { config.gains.kp = kp; },
                    [=](auto val) { return render(val); }));

    MenuEntry ki_entry(
            "Ki",
            make_controlled_value(
                    increments.gains.ki,
                    [&]() { return config.gains.ki; },
                    [&](KiType ki) { config.gains.ki = ki; },
                    [=](auto val) { return render(val); }));

    MenuEntry kd_entry(
            "Kd",
            make_controlled_value(
                    increments.gains.kd,
                    [&]() { return config.gains.kd; },
                    [&](KdType kd) { config.gains.kd = kd; },
                    [=](auto val) { return render(val); }));

    MenuEntry run_entry("Run", make_button_value([=]() { run_loop(); }));
    MenuEntry stop_entry("Stop", make_button_value([=]() { stop_loop(); }));

    return {
            std::move(tol_entry),
            std::move(vel_entry),
            std::move(accel_entry),
            std::move(kp_entry),
            std::move(ki_entry),
            std::move(kd_entry),
            std::move(run_entry),
            std::move(stop_entry),
    };
}

#endif // SPEED_CONTROL_MENU_HPP_
