
#include "purpl/vision.hpp"

#include "api.h"

namespace purpl {

Vision::Vision(uint8_t port, pros::vision_zero_e_t zero_point) :
    pros::Vision(port, zero_point) {}

Snapshot Vision::snapshot_all() {
    size_t total = get_object_count();
    std::vector<VisionObject> buffer(total);
    read_by_size(0, total, buffer.data());
    return buffer;
}

Snapshot Vision::snapshot_sig(uint32_t sig_id) {
    size_t count = 4;
    size_t size_id = 0;
    std::vector<VisionObject> buffer;
    while (true) {
        auto cur_size = buffer.size();
        buffer.resize(cur_size + count);
        auto read =
                read_by_sig(size_id, sig_id, count, buffer.data() + cur_size);
        if (read == count) {
            size_id += count;
            // Read more the next time
            count *= 1.5;
        } else if (read == PROS_ERR) {
            // TODO Return an error?
            return {{}};
        } else {
            // Shrink the vector back down
            buffer.resize(cur_size + read);
            return buffer;
        }
    }
}

Snapshot Vision::snapshot_code(uint32_t code_id) {
    size_t count = 4;
    size_t size_id = 0;
    std::vector<VisionObject> buffer;
    while (true) {
        auto cur_size = buffer.size();
        buffer.resize(cur_size + count);
        auto read =
                read_by_code(size_id, code_id, count, buffer.data() + cur_size);
        if (read == count) {
            size_id += count;
            // Read more the next time
            count *= 2;
        } else if (read == PROS_ERR) {
            // TODO Return an error?
            return {{}};
        } else {
            // Shrink the vector back down
            buffer.resize(cur_size + read);
            break;
        }
    }

    return buffer;
}

} // namespace purpl
