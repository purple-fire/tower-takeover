/**
 * Generic rate limiting controller.
 */

#pragma once

#ifndef PURPL_ACCEL_LIMIT_CONTROL_HPP_
#define PURPL_ACCEL_LIMIT_CONTROL_HPP_

#include "math.hpp"

#include <algorithm>
#include <utility>

namespace purpl {

/**
 * Filter to limit a value to within certain bounds.
 *
 * @tparam Time Type for time (deltas).
 * @tparam Input Type of the input (and output) of this controller.
 */
template <typename Time, typename Input>
class AccelLimitController {
public:
    /**
     * Type of acceleration of the input value.
     */
    using AccelType = decltype(std::declval<Input>() / std::declval<Time>());

    /**
     * Construct a AccelLimitController from its bounds.
     *
     * @param max Minimum and maximum bounds for the output acceleration.
     */
    AccelLimitController(AccelType max) : max_accel(max) {}

    /**
     * Sends the input to the output, limiting the rate of change.
     *
     * @param delta Time delta from the last call to this.
     * @param current Current input value.
     */
    Input step(Time delta, Input current) {
        const auto target_accel = (current - last_output) / delta;
        const auto abs_accel = std::min(purpl::abs(target_accel), max_accel);
        const auto accel = purpl::copysign(abs_accel, target_accel);
        last_output += accel * delta;
        return last_output;
    }

    /**
     * Maximum output acceleration.
     */
    AccelType max_accel;

private:
    /**
     * Last output value.
     */
    Input last_output;
};

} // namespace purpl

#endif // PURPL_ACCEL_LIMIT_CONTROL_HPP_
