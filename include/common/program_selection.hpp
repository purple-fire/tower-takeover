#pragma once

#ifndef PROGRAM_SELECTION_HPP_
#define PROGRAM_SELECTION_HPP_

enum class Program { red, blue, skills };

#if defined(PROG_RED)
constexpr Program PROGRAM = Program::red;
#elif defined(PROG_BLUE)
constexpr Program PROGRAM = Program::blue;
#elif defined(PROG_SKILLS)
constexpr Program PROGRAM = Program::skills;
#else
#error "No autonomous PROGRAM SPECIFIED. See README.md for instructions."
#endif

#endif // PROGRAM_SELECTION_HPP_
