#pragma once

#ifndef PURPL_ASYNC_WRAPPER_HPP_
#define PURPL_ASYNC_WRAPPER_HPP_

#include "pros/rtos.hpp"
#include "purpl/math.hpp"

namespace purpl {

/**
 * Helper class which can be extended to run a loop() function in a new task.
 *
 * This is meant to be extended (usually publically) with the type parameter
 * being the derived class.
 */
template <typename T>
class AsyncWrapper {
public:
    AsyncWrapper(T* that, const char* name = "") :
        task(trampoline, that, name) {
        // Don't start the task immediately
        task.suspend();
    }

    ~AsyncWrapper() {
        task.remove();
    }

    void start_task() {
        task.resume();
    }

    void suspend_task() {
        task.suspend();
    }

protected:
    pros::Task task;

private:
    static void trampoline(void* that) {
        static_cast<T*>(that)->loop();
    }
};

/**
 * Helper function for asynchronous loops and handle delaying between
 * iterations.
 *
 * @tparam F Callable which takes the amount of time since the last iteration as
 * a std::uint32_t number of milliseconds.
 *
 * @param delay Requested number of milliseconds to delay.
 * @param loop Function to perform 1 iteration of the loop.
 */
template <typename F>
void make_async_loop(std::uint32_t delay, F loop) {
    std::uint32_t prev_time = pros::millis();
    // Assume correct delay for first iteration since using a time of 0 will
    // often lead to division by 0.
    std::uint32_t actual_delay = delay;

    while (true) {
        loop(actual_delay);
        const std::uint32_t prev_prev_time = prev_time;
        pros::Task::delay_until(&prev_time, delay);
        actual_delay = prev_time - prev_prev_time;
    }
}

/**
 * Helper function for making wait_settled functions.
 *
 * @tparam T Type of the error value.
 * @tparam F Type of the loop callable.
 *
 * @param delay Requested amount of time to delay between checks.
 * @param duration Amount of time the system has to be within tolerance for the
 * loop to exit.
 * @param tolerance Tolerance value for the error.
 * @param loop Callable which returns the current error (difference from target
 * and desired value).
 */
template <typename T, typename F>
void make_wait_settled_func(
        std::uint32_t delay, std::uint32_t duration, T tolerance, F loop) {
    std::uint32_t steady_start = 0;

    while (true) {
        const auto err = loop();
        if (purpl::abs(err) < tolerance) {
            if (steady_start == 0) {
                steady_start = pros::millis();
            } else if (pros::millis() - steady_start >= duration) {
                return;
            }
        } else {
            steady_start = 0;
        }

        pros::Task::delay(delay);
    }
}

} // namespace purpl

#endif // PURPL_ASYNC_WRAPPER_HPP_
