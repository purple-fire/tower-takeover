
#pragma once

#ifndef PURPL_OBJECT_LOCATOR_HPP_
#define PURPL_OBJECT_LOCATOR_HPP_

#include "purpl/units.hpp"
#include "purpl/vision.hpp"

namespace purpl {

class ObjectLocator {
public:
    ObjectLocator(QLength width, QLength height);

    QLength get_distance(const VisionObject& object);
    QAngle get_yaw(const VisionObject& object);
    QAngle get_pitch(const VisionObject& object);

private:
    QLength width;
    QLength height;
};

} // namespace purpl

#endif // PURPL_OBJECT_LOCATOR_HPP_
