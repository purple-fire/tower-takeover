#pragma once

#ifndef INTAKE_ARMS_HPP_
#define INTAKE_ARMS_HPP_

#include "pros/rtos.hpp"
#include "purpl/async_wrapper.hpp"
#include "purpl/math.hpp"
#include "purpl/potentiometer.hpp"
#include "purpl/units.hpp"

/**
 * Class to manage the arms for the intake.
 *
 * @tparam M Type of motor(s) for each arm.
 */
template <class M>
class IntakeArms {
public:
    IntakeArms(
            M&& left_motor,
            M&& right_motor,
            purpl::Pot&& pot,
            purpl::QAngularVelocity max_speed) :
        left_motor(std::move(left_motor)),
        right_motor(std::move(right_motor)),
        pot(std::move(pot)),
        max_speed(max_speed) {
        left_motor.tare_position();
        right_motor.tare_position();

        // Ths has to be here because potentiometers are not fully initialized
        // for a bit. Without this, the pot always returns 0 when called here.
        pros::delay(1000);

        left_motor.set_zero_position(pot.get_angle());
        right_motor.set_zero_position(pot.get_angle());

        encoder_drift = pot.get_angle() - left_motor.get_position();
    }

    void move_velocity(purpl::QAngularVelocity vel) {
        left_motor.move_velocity(vel);
        right_motor.move_velocity(vel);
    }

    void set_target(purpl::QAngle position) {
        encoder_drift = pot.get_angle() - left_motor.get_position();

        left_motor.move_absolute(position - encoder_drift, max_speed);
        right_motor.move_absolute(position - encoder_drift, max_speed);
    }

    purpl::QAngle get_pot_angle() const {
        return pot.get_angle();
    }

    std::pair<purpl::QAngle, purpl::QAngle> get_angles() const {
        return {
                left_motor.get_position() + encoder_drift,
                right_motor.get_position() + encoder_drift,
        };
    }

    purpl::QAngularVelocity get_max_speed() {
        return max_speed;
    }

    void wait_arms_settled(
            purpl::QAngle tol = DEFAULT_ARMS_TOLERANCE,
            purpl::QTime duration = DEFAULT_SETTLE_DURATION) {
        const std::uint32_t dur_millis = duration.convert(purpl::millisecond);
        purpl::make_wait_settled_func(LOOP_DELAY, dur_millis, tol, [&]() {
            const auto left_motor_angle = left_motor.get_position() -
                                          left_motor.get_target_position();

            const auto right_motor_angle = right_motor.get_position() -
                                           right_motor.get_target_position();

            return std::max(
                    purpl::abs(left_motor_angle),
                    purpl::abs(right_motor_angle));
        });
    }

private:
    M left_motor;
    M right_motor;

    static constexpr std::uint32_t LOOP_DELAY = 20;
    static constexpr purpl::QAngle DEFAULT_ARMS_TOLERANCE = 1 * purpl::degree;
    static constexpr purpl::QTime DEFAULT_SETTLE_DURATION = 0.2 * purpl::second;

    /**
     * Potentiometer on one side of the arms.
     */
    purpl::Pot pot;

    /**
     * Drift from the potentiometer and motor encoder values.
     *
     * We assume that both motors have the same drift.
     *
     * Initialized to 0 (default) since the motor encoders are initially
     * calibrated.
     */
    purpl::QAngle encoder_drift;

    const purpl::QAngularVelocity max_speed;
};

#endif // INTAKE_ARMS_HPP_
