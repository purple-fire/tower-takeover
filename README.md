# Tower Takeover

This is the code for the 2019-2020 VEXU season for Purple Fire Robotics or
Florida Polytechnic University.

"Badcock" and "More" are the 24" and 15" robots, respectively. Code they
share is in the `src/common` `include/subsystems` and directories while
robot-specific code is in `badcock/src` and `more/src` respectively.

For the time being, this repository also contains the development code for
[PURPL](https://gitlab.com/purple-fire/purpl/) (PURPL Ultimate Robotics
Programming Library). Once the core part of it is stabalized, the code will
move into its own repository so that it can be shared amoung other projects
in future years.


## Building/Running

We use the [PROS](https://pros.cs.purdue.edu) OS /
toolchain. Instructions for installing and using it can be found at
https://pros.cs.purdue.edu/v5/getting-started/index.html.

Each robot has 3 autonomous routines: red side, blue side, and skills. If
un-specified, the skills program will be used, but the program can be set by
giving the options `PROGRAM=RED`, `PROGRAM=BLUE`, or `PROGRAM=SKILLS` when
running `make` (or `prosv5 make`). You should do a full build (`make all`)
before chaning to a different program to ensure that the autonomous routine is
re-compiled.

Note: If you're using the PROS atom plugin, you must navigate to a file in
one of the robot-specific directories (not the common or header files) to
build one of the projects since that is where the `project.pros` files exist.


## Coding Guidelines

- Try to make sure code is formatted according to the coding style defined in
  the `.editorconfig` and `.clang-format` files. The easiest way to do this
  is by running `make format` over code before committing it to Git (or,
  better yet, have your editor to it before every save). A basic summary of
  the style is:
  - 4 spaces for indentation
  - Keep lines under 80 characters. Generally, indent line continuations twice
    (don't indent after type or template declartions).
  - Type names in CamelCase
  - Variable/function names in snake_case
  - Global constants in CAPITAL\_CASE
- Try to add doc comments to classes/functions/variables describing their
  functionality. Doc comments look like (see http://www.doxygen.nl/ for a
  full description):
``` c++
/**
* Brief description.
*
* More in-depth description if necessary.
*
* @param name Description of the name parameter.
* @return Description of the returned value.
*/
```
- Use the units library in PURPL. The `purpl/units.hpp` file defines unit types
  which do compile-time dimensionaly analysis to make sure mathematical
  operations dealing with real-world values make sence. For example,
  they won't allow you to add a time and a distance value together. Most
  (if not all) functions in PURPL take/return units when possible to (1)
  be explicit and (2) prevent some easy mistakes.
