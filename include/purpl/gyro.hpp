#pragma once

#ifndef PURPL_GYRO_HPP_
#define PURPL_GYRO_HPP_

#include "pros/adi.hpp"
#include "units.hpp"

namespace purpl {

class Gyro : private pros::ADIGyro {
public:
    Gyro(std::uint8_t port, double multiplier = 1.0);

    Gyro(const Gyro&) = delete;
    Gyro& operator=(const Gyro&) = delete;

    Gyro(Gyro&& other) = default;

    QAngle get_angle() const;

    void reset();
};

} // namespace purpl

#endif // PURPL_GYRO_HPP_
