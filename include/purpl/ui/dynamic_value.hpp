#pragma once

#ifndef PURPL_UI_DYNAMIC_VALUE_HPP_
#define PURPL_UI_DYNAMIC_VALUE_HPP_

#include <string>

namespace purpl {

namespace ui {

/**
 * UI element for a value from the outside world that may change over time and
 * may be changed by this element's event handler.
 */
template <typename Event, typename Context>
class DynamicValue {
public:
    virtual ~DynamicValue() = default;

    /**
     * Whether this value needs updating since the last render.
     *
     * By default, this always returns false. If this value reports some
     * external value, this should always return true so that the value
     * shown on screen is always up-to-date.
     */
    virtual bool needs_update() const {
        return false;
    }

    /**
     * Respond to a button event.
     * This is given a reference to the containing menu so that it can
     * push/pop submenus.
     *
     * By default, this does nothing.
     */
    virtual void on_event(Event event, Context& context) {}

    /**
     * Render the value as a string to be shown on screen.
     *
     * By default, this returns an empty string.
     */
    virtual std::string render() const {
        return "";
    }
};

/**
 * DynamicValue for an value which changes continously.
 *
 * The value is retrieved from a callable object and converted into a string
 * using the Renderer. This is intended to be used with numbers, but will
 * work with anything for which operator!= is overloaded.
 *
 * The reason for distinguishing Getter and Renderer is so that cached values
 * can be compared as the raw value, which should be quicker than comparing
 * strings.
 */
template <typename Event, typename Context, typename Getter, typename Renderer>
class DynamicContinuousValue : public DynamicValue<Event, Context> {
public:
    /**
     * Return value from the Getter function.
     */
    using ValueType = decltype(std::declval<Getter>()());

    /**
     * Constructs a DynamicContinuousValue from it's callback.
     */
    DynamicContinuousValue(Getter&& func, Renderer&& renderer) :
        get_value(func), render_value(renderer) {}

    /**
     * Returns whether the current value has been changed since the last render.
     */
    virtual bool needs_update() const {
        return get_value() != last_value;
    }

    /**
     * Convert the output of func with std::to_string().
     */
    virtual std::string render() const {
        last_value = get_value();
        return render_value(last_value);
    }

private:
    Getter get_value;
    Renderer render_value;

    /**
     * Cache of the last shown value so that this doesn't have to be re-rendered
     * when the value has not changed.
     *
     * This is default-initialized to start.
     *
     * TODO Should another flag (or maybe std::optional) be used in addition to
     * this so that needs_update() returns false on the first render? Or will
     * this always be rendered the first time anyway.
     */
    mutable ValueType last_value;
};

} // namespace ui

} // namespace purpl

#endif // PURPL_UI_DYNAMIC_VALUE_HPP_
