#pragma once

#ifndef RECLINER_HPP_
#define RECLINER_HPP_

#include "purpl/math.hpp"
#include "purpl/motor.hpp"
#include "purpl/potentiometer.hpp"
#include "purpl/units.hpp"

#include <algorithm>
#include <cmath>
#include <utility>

enum class ReclinerState { backward, free_arms, forward, off };

template <class M>
class Recliner {
public:
    Recliner(
            M&& motor,
            purpl::QAngularVelocity max_speed,
            purpl::Pot&& pot,
            purpl::QAngle pot_forward_max,
            purpl::QAngle pot_backward_max,
            purpl::QAngle pot_arm_value) :
        motor(std::move(motor)),
        max_speed{max_speed},
        pot(std::move(pot)),
        forward_max{pot_forward_max},
        backward_max{pot_backward_max},
        arm_value{pot_arm_value} {}

    Recliner(const Recliner&) = delete;
    Recliner& operator=(const Recliner& other) = delete;
    Recliner(Recliner&&) = default;

    void set_state(ReclinerState state) {
        this->state = state;
    }

    ReclinerState get_state() const {
        return state;
    }

    void set_override(bool override_sensors) {
        this->override_sensors = override_sensors;
    }

    bool get_override() {
        return override_sensors;
    }

    void step() {
        if (!override_sensors) {
            if (state == ReclinerState::forward &&
                pot.get_angle() > forward_max) {
                const auto full_span = backward_max - forward_max;
                const auto proportion =
                        (get_potentiometer_angle() - forward_max) / full_span;
                const auto scalar = std::pow(
                        proportion.convert(purpl::number), SCALING_EXPONENT);

                motor.move_velocity((scalar + END_SPEED_PROP) * max_speed);
            } else if (
                    state == ReclinerState::backward &&
                    pot.get_angle() < backward_max) {
                motor.move_velocity(-max_speed);
            } else if (state == ReclinerState::free_arms) {
                // Move at full speed in the proper direction until we meet
                // the tolerance.
                const auto move_vec = get_potentiometer_angle() - arm_value;
                if (purpl::abs(move_vec) < FREE_ARMS_TOL) {
                    motor.move_velocity(0 * purpl::rpm);
                } else {
                    motor.move_velocity(purpl::copysign(max_speed, move_vec));
                }
            } else {
                motor.move_velocity(0 * purpl::rpm);
            }
        } else {
            if (state == ReclinerState::forward) {
                motor.move_velocity(max_speed);
            } else if (state == ReclinerState::backward) {
                motor.move_velocity(-max_speed);
            } else {
                motor.move_velocity(0 * purpl::rpm);
            }
        }
    }

    purpl::QAngle get_potentiometer_angle() const {
        return pot.get_angle();
    }

private:
    M motor;
    purpl::Pot pot;
    bool override_sensors = false;

    static constexpr double SCALING_EXPONENT = 0.5;
    static constexpr double END_SPEED_PROP = 0.05;

    static constexpr purpl::QAngle FREE_ARMS_TOL = 5 * purpl::degree;

protected:
    const purpl::QAngularVelocity max_speed;

    const purpl::QAngle forward_max;
    const purpl::QAngle backward_max;
    const purpl::QAngle arm_value;

    ReclinerState state = ReclinerState::off;
};

#endif
