
#include "purpl/potentiometer.hpp"

#include "pros/adi.hpp"

namespace purpl {

Pot::Pot(std::uint8_t port, bool reversed, QAngle multiplier, QAngle offset) :
    pros::ADIPotentiometer(port),
    reversed(reversed),
    multiplier(multiplier),
    offset(offset) {}

QAngle Pot::get_angle() const {
    return convert_raw(get_value());
}

QAngle Pot::convert_raw(std::int32_t raw) const {
    if (reversed) {
        raw = 4095 - raw;
    }
    return raw * multiplier + offset;
}

CalibratedPot::CalibratedPot(
        std::uint8_t port, bool reversed, QAngle multiplier, QAngle offset) :
    Pot(port, reversed, multiplier, offset) {
    pros::ADIPotentiometer::calibrate();
}

QAngle CalibratedPot::get_angle() const {
    return convert_raw(get_value_calibrated());
}

} // namespace purpl
