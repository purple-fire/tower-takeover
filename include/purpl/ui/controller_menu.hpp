#pragma once

#ifndef PURPL_UI_CONTROLLER_MENU_HPP_
#define PURPL_UI_CONTROLLER_MENU_HPP_

#include "pros/misc.hpp"
#include "purpl/async_wrapper.hpp"
#include "purpl/ui/dynamic_value.hpp"

#include <algorithm>
#include <memory>
#include <stack>
#include <string>
#include <vector>

namespace purpl {

namespace ui {

// Pre-declare since the entries reference it
class ControllerMenu;

/**
 * Mirror of pros::controller_digital_e_t using enum class.
 */
enum class ControllerButton {
    l1 = pros::E_CONTROLLER_DIGITAL_L1,
    l2 = pros::E_CONTROLLER_DIGITAL_L2,
    r1 = pros::E_CONTROLLER_DIGITAL_R1,
    r2 = pros::E_CONTROLLER_DIGITAL_R2,
    up = pros::E_CONTROLLER_DIGITAL_UP,
    down = pros::E_CONTROLLER_DIGITAL_DOWN,
    left = pros::E_CONTROLLER_DIGITAL_LEFT,
    right = pros::E_CONTROLLER_DIGITAL_RIGHT,
    x = pros::E_CONTROLLER_DIGITAL_X,
    b = pros::E_CONTROLLER_DIGITAL_B,
    y = pros::E_CONTROLLER_DIGITAL_Y,
    a = pros::E_CONTROLLER_DIGITAL_A,
};

// This can't be constexpr since std::initializer_list's constructor isn't.
#define ALL_CONTROLLER_BUTTONS                                                 \
    {                                                                          \
        pros::E_CONTROLLER_DIGITAL_L1, pros::E_CONTROLLER_DIGITAL_L2,          \
                pros::E_CONTROLLER_DIGITAL_R1, pros::E_CONTROLLER_DIGITAL_R2,  \
                pros::E_CONTROLLER_DIGITAL_UP,                                 \
                pros::E_CONTROLLER_DIGITAL_DOWN,                               \
                pros::E_CONTROLLER_DIGITAL_LEFT,                               \
                pros::E_CONTROLLER_DIGITAL_RIGHT,                              \
                pros::E_CONTROLLER_DIGITAL_X, pros::E_CONTROLLER_DIGITAL_B,    \
                pros::E_CONTROLLER_DIGITAL_Y, pros::E_CONTROLLER_DIGITAL_A     \
    }

/**
 * Width (in characters) fo the controller LCD.
 */
constexpr std::uint8_t CONTROLLER_WIDTH = 15;

/**
 * Height (in characters) of the controller LCD.
 */
constexpr std::uint8_t CONTROLLER_HEIGHT = 3;

class MenuEntry {
public:
    /**
     * Template specification of DynamicValue for controller menus.
     */
    using Value = DynamicValue<ControllerButton, ControllerMenu>;

    /**
     * Construts a MenuEntry.
     *
     * @param label Label for the entry. This should be no more than 8
     * characters long.
     * @param value DynamicValue instance.
     */
    MenuEntry(std::string label, std::unique_ptr<Value> value);

    /**
     * Constructs a MenuEntry.
     *
     * TODO This seems to cause the compiler to loop indefinitely when used.
     */
    /*
    template <typename V>
    MenuEntry(std::string label, V&& value) :
        MenuEntry(
                label,
                std::make_unique<std::remove_reference_t<V>>(
                        std::move(value))) {}
    */

    /**
     * Get this entry's label.
     *
     * This should always be no more than 8 characters long.
     */
    const std::string& render_label() const;

    /**
     * Whether this entry's value needs updating.
     *
     * @see DynamicValue::needs_update()
     */
    bool needs_update() const;

    /**
     * Get a string representation of the value.
     *
     * @see DynamicValue::render()
     */
    std::string render_value() const;

    /**
     * Forwards an event to it's value.
     *
     * @see DynamicValue::on_event()
     */
    void on_event(ControllerButton event, ControllerMenu& root_menu);

private:
    /**
     * Label for the entry.
     *
     * This should be short as there is limited space on the controller screen
     * (it will be cutoff at 8 characters).
     */
    std::string label;

    /**
     * DynamicValue showing the menu entry value and responding to events
     * (if any).
     */
    std::unique_ptr<Value> value;
};

/**
 * Single level of a menu (i.e. excluding submenus).
 */
class Menu {
public:
    /**
     * Constructs a Menu from a vector of MenuEntry's.
     */
    Menu(std::vector<MenuEntry>&& entries);

    /**
     * Constructs a Menu from MenuEntry's.
     */
    template <typename... Entries>
    Menu(Entries&&... entries) {
        // This seems to be the only way to initialize the vector without
        // calling the (implicitly deleted) MenuEntry copy constructor.
        this->entries.reserve(sizeof...(Entries));
        (this->entries.emplace_back(std::move(entries)), ...);
    }

    /**
     * Constructs a menu containing the entries for 2 other menus.
     */
    Menu(Menu&& first, Menu&& second);

    /**
     * Whether this menu needs to be re-rendered.
     *
     * This may be because it's selection has changed or because one of the
     * visible entries needs updating.
     */
    bool needs_update() const;

    /**
     * Renders the menu to a controller.
     *
     * TODO Should this assume the screen has not changed since the last render?
     * Maybe take an extra parameter for this?
     *
     * @param controller Controller screen to draw to.
     */
    void render(pros::Controller& controller) const;

    /**
     * Intercepts events to move the entry selection and forwards other events
     * to the selected entry.
     *
     * UP and DOWN move the selection. All other events are forwarded.
     *
     * @param event Button press event.
     * @param root_menu ControllerMenu instance displaying this menu. This is
     * passed so that a menu entry can spawn a submenu.
     */
    void on_event(ControllerButton event, ControllerMenu& root_menu);

private:
    /**
     * Helper to render a single menu entry.
     *
     * @param controller Controller screen to draw to.
     * @param line Line to draw the entry on [0-2].
     * @param entry Entry to draw.
     * @param is_current Whether this entry is the current selected one.
     */
    void render_entry(
            pros::Controller& controller,
            std::uint8_t line,
            const MenuEntry& entry,
            bool is_current) const;

    /**
     * Entries in the menu.
     *
     * TODO Wrap this in shared_ptr so that Menu is copyable?
     * TODO Split this off into a separate struct/class (i.e. MenuConfig) so
     * that Menus can be stored without storing and saving the selection and
     * other state?
     */
    std::vector<MenuEntry> entries;

    /**
     * Current selected index in entries.
     */
    std::uint8_t current = 0;

    /**
     * Current scroll offset of the top entry shown.
     */
    std::uint8_t scroll_pos = 0;

    /**
     * Whether the current selection has changed since the last render.
     *
     * Mutable so that it can be modified (cleared) in render().
     */
    mutable bool selection_dirty;
};

/**
 * Class managing a menu displayed on the controller.
 */
class ControllerMenu : public AsyncWrapper<ControllerMenu> {
public:
    /**
     * Constructs a controller menu.
     *
     * @param controller Controller to draw to and take inputs from.
     * @param initial_menu Initial and base menu.
     */
    ControllerMenu(pros::Controller controller, Menu&& initial_menu);

    /**
     * Push a new menu to the stack.
     *
     * This will be displayed on the next render (unless it is popped before
     * then).
     */
    void push_menu(std::shared_ptr<Menu> new_menu);

    /**
     * Go back to the previous menu (if any).
     */
    void pop_menu();

    /**
     * Poll for new button presses and run appropriate actions.
     *
     * If inhibit_input is set, this does nothing.
     * needs_update() should be called to test if an upate is required and then
     * render() should be called.
     */
    void poll_events();

    /**
     * Check if button is held
     */
    bool is_button_held(ControllerButton btn);

    /**
     * Gets whether input is inhibitted.
     */
    bool get_input_inhibit() const;

    /**
     * Toggles whether input is inhibitted.
     *
     * @see inhibit_input
     */
    void toggle_input_inhibit();

    /**
     * Whether the current menu needs to be re-rendered.
     */
    bool needs_update() const;

    /**
     * Renders the menu to the screen.
     *
     * This is non-const so that it can write to the controller.
     */
    void render();

    /**
     * Render the menu and poll for events.
     *
     * This is meant to be run in a separate thread via
     * AsyncWrapper::start_task() (and stopped via AsyncWrapper::stop_task()).
     * While this task can be run while other tasks are run, care needs to be
     * taken that bindings do not overlap. It might be a good idea to put other
     * tasks taking input from the controller on hold while this is executing.
     */
    void loop();

private:
    /**
     * Helper to intercept/forward events.
     *
     * Intercepts B to go back, passes everything else through.
     */
    void on_event(ControllerButton event);

    /**
     * Controller to render to and take input from.
     */
    pros::Controller controller;

    /**
     * Stack of menus.
     *
     * The top of the stack is the menu currently showing. This will never be
     * empty, so there will always be a menu showing.
     *
     * TODO Make this store Menus by-value? MenuEntry's would still have to be
     * in shared_ptr's since they cannot be copied, but the rest of the Menu
     * state should probably not able to be referenced from outside.
     */
    std::stack<std::shared_ptr<Menu>> menu_stack;

    /**
     * Whether a new menu has been pushed on / popped off the stack.
     */
    bool menu_dirty = false;

    /**
     * Lowest controller button value.
     */
    static constexpr std::size_t MIN_CONTROLLER_BUTTON =
            std::min(ALL_CONTROLLER_BUTTONS);

    /**
     * Highest controller button value.
     */
    static constexpr std::size_t MAX_CONTROLLER_BUTTON =
            std::max(ALL_CONTROLLER_BUTTONS);

    /**
     * Total number of controller buttons.
     */
    static constexpr std::size_t CONTROLLER_BUTTON_COUNT =
            MAX_CONTROLLER_BUTTON - MIN_CONTROLLER_BUTTON;

    /**
     * Since pros::Controller::get_digital_new_press() is not thread-safe, we
     * have to maintain button states locally.
     *
     * Initialized to all false.
     */
    std::array<bool, CONTROLLER_BUTTON_COUNT> button_states{};

    /**
     * Whether to not the menu should be reading buttons.
     *
     * This is here to allow using buttons the controller uses to control the
     * robot when not using the controller.
     */
    bool input_inhibit = true;
};

/**
 * DynamicValue for a submenu.
 *
 * This has no rendered value and so never needs to be updated.
 */
class SubmenuValue : public DynamicValue<ControllerButton, ControllerMenu> {
public:
    /**
     * Constructs a SubmenuValue.
     *
     * @param menu Submenu to use.
     */
    SubmenuValue(Menu&& menu);

    /**
     * On A or RIGHT, push the submenu to the menu stack.
     *
     * @param event Event button to check.
     * @param root_menu ControllerMenu to push the submenu to.
     */
    virtual void on_event(ControllerButton event, ControllerMenu& root_menu);

private:
    /**
     * Submenu.
     */
    std::shared_ptr<Menu> menu;
};

std::unique_ptr<SubmenuValue> make_submenu_value(Menu&& menu);

/**
 * DynamicValue representing a button (has no value, just handles input events).
 */
template <typename F>
class ButtonValue : public DynamicValue<ControllerButton, ControllerMenu> {
public:
    ButtonValue(F&& on_press) : on_press(std::move(on_press)) {}

    /**
     * Calls the function when A is pressed.
     */
    virtual void on_event(ControllerButton event, ControllerMenu& root_menu) {
        switch (event) {
        case ControllerButton::right:
        case ControllerButton::a:
            on_press();
            break;
        default:
            break;
        }
    }

private:
    F on_press;
};

template <typename F>
std::unique_ptr<ButtonValue<F>> make_button_value(F&& on_press) {
    return std::make_unique<ButtonValue<F>>(std::forward<F>(on_press));
}

/**
 * DynamicValue for an value which is changed (only) via this object.
 *
 * This responds to the following buttons:
 * - right: increment value
 * - left: decrement value
 *
 * @tparam T Type of the controlled value. This must have overloads for
 * operator+ and operator- (i.e. should be number-like).
 * @tparam Getter Function-like which gets the value: T operator()().
 * @tpatam Setter Function-like which sets the value: void operator()(T).
 */
template <typename T, typename Getter, typename Setter, typename Renderer>
class DynamicControlledValue :
    public DynamicValue<ControllerButton, ControllerMenu> {
public:
    /**
     * Constructs a DynamicControlledValue from it's increment value and
     * getter/setter.
     */
    DynamicControlledValue(
            T increment,
            Getter&& getter,
            Setter&& setter,
            Renderer&& renderer) :
        increment(increment),
        get_value(std::move(getter)),
        set_value(std::move(setter)),
        render_value(std::move(renderer)) {}

    /**
     * Returns true after the value has been set.
     */
    virtual bool needs_update() const {
        return value_dirty;
    }

    /**
     * Handles input events.
     *
     * Right: increment value
     * Left: decrement value
     */
    virtual void on_event(ControllerButton event, ControllerMenu& menu) {
        bool is_modifier_on = menu.is_button_held(ControllerButton::l1);

        auto modified_increment = is_modifier_on ? 10 * increment : increment;

        switch (event) {
        case ControllerButton::right:
            set_value(get_value() + modified_increment);
            break;
        case ControllerButton::left:
            set_value(get_value() - modified_increment);
            break;
        default:
            // Don't set value_dirty
            return;
        }

        value_dirty = true;
    }

    /**
     * Convert the output of func with render_value().
     */
    virtual std::string render() const {
        value_dirty = false;
        return render_value(get_value());
    }

private:
    /**
     * Amount to change when the Left and Right buttons are pressed.
     */
    const T increment;

    Getter get_value;
    Setter set_value;
    Renderer render_value;

    /**
     * Whether the value has been set but not re-rendered.
     */
    mutable bool value_dirty = true;
};

template <typename T, typename Getter, typename Setter, typename Renderer>
std::unique_ptr<DynamicControlledValue<T, Getter, Setter, Renderer>>
make_controlled_value(
        T increment, Getter&& getter, Setter&& setter, Renderer&& renderer) {
    return std::make_unique<
            DynamicControlledValue<T, Getter, Setter, Renderer>>(
            increment,
            std::forward<Getter>(getter),
            std::forward<Setter>(setter),
            std::forward<Renderer>(renderer));
}

/**
 * Make a DynamicControlledValue using std::to_string to display values.
 */
template <typename T, typename Getter, typename Setter>
auto make_controlled_value(T increment, Getter&& getter, Setter&& setter) {
    return make_controlled_value(
            increment,
            std::forward<Getter>(getter),
            std::forward<Setter>(setter),
            [](auto v) { return std::to_string(v); });
}

template <typename Getter, typename Renderer>
std::unique_ptr<DynamicContinuousValue<
        ControllerButton,
        ControllerMenu,
        Getter,
        Renderer>>
make_continuous_value(Getter&& getter, Renderer&& renderer) {
    return std::make_unique<DynamicContinuousValue<
            ControllerButton,
            ControllerMenu,
            Getter,
            Renderer>>(
            std::forward<Getter>(getter), std::forward<Renderer>(renderer));
}

/**
 * Make a DynamicControlledValue using std::to_string to display values.
 */
template <typename Getter>
auto make_continuous_value(Getter&& getter) {
    return make_continuous_value(std::forward<Getter>(getter), [](auto v) {
        return std::to_string(v);
    });
}

} // namespace ui

} // namespace purpl

#endif // PURPL_UI_CONTROLLER_MENU_HPP_
